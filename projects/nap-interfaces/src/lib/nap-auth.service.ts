import { Injectable } from '@angular/core';
import { NAPConnectionService } from './nap-connection.service';
import { NapResponse, NapResourcesAllowedI, NAPLoginOptions, NAPLoginIdentities, NAPResetPasswordOptions } from './napi.interfaces';
import { Subject } from 'rxjs';


@Injectable()
export class NAPAuthService {
    
    private authUser: any = {};
    public userChanged: Subject<void> = new Subject();

    set authenticated(auth: boolean) {this.conn.authenticated = auth;}
    get authenticated(): boolean {return this.conn.authenticated;}


    private _resourcesAllowed: NapResourcesAllowedI = {};
    set resourcesAllowed(resAllw: NapResourcesAllowedI) {
        if (resAllw === this._resourcesAllowed) return;
        this._resourcesAllowed = resAllw;
        this.resourcesAllowedChange.next(this._resourcesAllowed);
    }
    get resourcesAllowed(): NapResourcesAllowedI {
        return this._resourcesAllowed;
    }
    resourcesAllowedChange: Subject<NapResourcesAllowedI> = new Subject();

    sessionEngine: string;

    
    constructor(public conn: NAPConnectionService) {}

    checkAuthenticated(): Promise<boolean> {
        return new Promise((resolve) => {
            this.read().then(() => {
                if (this.authenticated) resolve(true);
                else resolve(false);
            });
        });
    }

    getUser() {
        if (!this.authenticated) return {};
        if (this.authUser.email) return this.authUser;
    }

    setUser(data: object) {
        Object.assign(this.authUser, data);
        this.userChanged.next();
    }

    ///////
    ///////  AUTH
    ///////
    login<T = any>(data: NAPLoginIdentities, remember?: boolean, options?: NAPLoginOptions): Promise<NapResponse<T>> {
        if (this.sessionEngine && !options) options = {sessionEngine: this.sessionEngine};
        if (this.sessionEngine && options && !options.sessionEngine) options.sessionEngine = this.sessionEngine;
        const requestBody = {operation: "login", remember: remember, options: options};
        Object.assign(requestBody, data);
        return this.conn.makeAuthRequest(requestBody);
    }
    
    logout(options?: NAPLoginOptions) {
        const requestBody = {operation: "logout", options: options};
        return this.conn.makeAuthRequest(requestBody);
    }
    
    recoverPasswordByEmail(email: string, options?: NAPResetPasswordOptions): Promise<NapResponse<any>> {
        const requestBody = {operation: "recoverPasswordByEmail", email: email, originUrl: window.location.origin, options: options};
        return this.conn.makeAuthRequest(requestBody);
    }
    recoverPasswordBySms(phone: string): Promise<NapResponse<any>> {
        const requestBody = {operation: "recoverPasswordBySms", phone: phone};
        return this.conn.makeAuthRequest(requestBody);
    }
    checkToken(token: string): Promise<NapResponse<any>> {
        const requestBody = {operation: "checkToken", token: token};
        return this.conn.makeAuthRequest(requestBody);
    }
    setPasswordForToken(token: string, password: string, options?: NAPLoginOptions): Promise<NapResponse<any>>  {
        const requestBody = {operation: "setPasswordForToken", token: token, password: password, options: options};
        return this.conn.makeAuthRequest(requestBody);
    }
    
    // update<T = any>(data: object): Promise<NapResponse<T>> {
    //     const requestBody = {operation: "update", data: data};
    //     return this.conn.makeAuthRequest(requestBody);
    // }

    read<T = any>(): Promise<NapResponse<T>> {
        const requestBody = {operation: "read"};
        const prom = this.conn.makeAuthRequest(requestBody);
        prom.then((resp) => {
            if (resp.status == "success") {
                let userChanged = false;
                if (JSON.stringify(this.authUser) !== JSON.stringify(resp.data)) userChanged = true;
                this.authUser = resp.data;
                this.resourcesAllowed = resp.resourcesAllowed;
                if (userChanged) this.userChanged.next();
            }
        })
        return prom;
    }
    
}
