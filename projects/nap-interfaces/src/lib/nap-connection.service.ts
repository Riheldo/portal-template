import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { NapResponse, NapAggregation, NAPMiddleware, NAPMiddlewaresProfiles, NAPListOptions, NAPReadOptions, NAPInsertOptions, NAPUpdateOptions, NAPDeleteOptions, NAPAggregateOptions } from './napi.interfaces';

export enum NapMidExecLapse {
    onStart = "onStart",
    onEnd = "onEnd",
    always = "always"
}

export class NAPConnectionModel {
    
    constructor(private conn: NAPConnectionService, private connAuthorization: string) {}

    middlewaresBeforeConn: Array<NAPMiddleware> = [];
    middlewareAfterConn: Array<NAPMiddleware> = [];

    executionBefore(o) {
        let i = 0;
        for (const middleware of this.middlewaresBeforeConn) {
            o = middleware({data: o, lapse: NapMidExecLapse.onStart, round: i});
            i++;
        }
        return o;
    }
    executionAfter(o) {
        let i = 0;
        for (const middleware of this.middlewareAfterConn) {
            o = middleware({data: o, lapse: NapMidExecLapse.onEnd, round: i});
            i++;
        }
        return o;
    }

    list<T = any, R = NapResponse<Array<T>>>(resource: string, filter?: Array<any>, options?: NAPListOptions): Promise<R> {
        let reqBody = {resource: resource, operation: "list", filter: filter, options: options, useAuth: this.connAuthorization};
        reqBody = this.executionBefore(reqBody);
        return this.conn.makeResourceRequest(reqBody).then((resp) => {
            return this.executionAfter(resp)
        });
    }
    read<T = any, R = NapResponse<Array<T>>>(resource: string, filter?: Array<any>, options?: NAPReadOptions): Promise<R> {
        let reqBody = {resource: resource, operation: "read", filter: filter, options: options, useAuth: this.connAuthorization};
        reqBody = this.executionBefore(reqBody);
        return this.conn.makeResourceRequest(reqBody).then((resp) => {
            return this.executionAfter(resp)
        });
    }
    insert<T = any, R = NapResponse<T>>(resource: string, data: T, options?: NAPInsertOptions): Promise<R> {
        let reqBody = {resource: resource, operation: "insert", data: data, useAuth: this.connAuthorization, options: options};
        reqBody = this.executionBefore(reqBody);
        return this.conn.makeResourceRequest(reqBody).then((resp) => {
            return this.executionAfter(resp)
        });
    }
    update<T = any, R = NapResponse<T>>(resource: string, data: T, filter: Array<any>, options?: NAPUpdateOptions): Promise<R> {
        let reqBody = {resource: resource, operation: "update", data: data, filter: filter, useAuth: this.connAuthorization, options: options};
        reqBody = this.executionBefore(reqBody);
        return this.conn.makeResourceRequest(reqBody).then((resp) => {
            return this.executionAfter(resp)
        });
    }
    delete<T = any, R = NapResponse<T>>(resource: string, filter: Array<any>, options?: NAPDeleteOptions): Promise<R> {
        let reqBody = {resource: resource, operation: "delete", filter: filter, useAuth: this.connAuthorization, options: options};
        reqBody = this.executionBefore(reqBody);
        return this.conn.makeResourceRequest(reqBody).then((resp) => {
            return this.executionAfter(resp)
        });
    }
    aggregate<T = any, R = NapResponse<T>>(resource: string, aggregation: NapAggregation, filter?: Array<any>, options?: NAPAggregateOptions): Promise<R> {
        let reqBody = {resource: resource, operation: "aggregate", aggregation: aggregation, filter: filter, useAuth: this.connAuthorization, options: options};
        reqBody = this.executionBefore(reqBody);
        return this.conn.makeResourceRequest(reqBody).then((resp) => {
            return this.executionAfter(resp)
        });
    }

    //
    //// MID OPERATIONS
    //

    with(executionOf: string | NAPMiddleware | Array<NAPMiddleware>, midPosition: NapMidExecLapse = NapMidExecLapse.always): NAPConnectionModel {

        if (typeof executionOf === "string") {
            if (!this.conn.middlewaresBeforeProfile[executionOf] && !this.conn.middlewaresAfterProfile[executionOf]) {
                console.warn("Profile "+executionOf+" not found! Connection will procede ignoring this profile.");
                return this;
            }
            if (midPosition === NapMidExecLapse.always || midPosition === NapMidExecLapse.onStart) this.middlewaresBeforeConn = this.middlewaresBeforeConn.concat(this.conn.middlewaresBeforeProfile[executionOf] || []);
            if (midPosition === NapMidExecLapse.always || midPosition === NapMidExecLapse.onEnd) this.middlewareAfterConn = this.middlewareAfterConn.concat(this.conn.middlewaresAfterProfile[executionOf] || []);
            return this;
        }

        if (executionOf instanceof Array) {
            if (midPosition === NapMidExecLapse.always || midPosition === NapMidExecLapse.onStart) this.middlewaresBeforeConn = this.middlewaresBeforeConn.concat(executionOf);
            if (midPosition === NapMidExecLapse.always || midPosition === NapMidExecLapse.onEnd) this.middlewareAfterConn = this.middlewareAfterConn.concat(executionOf);
            return this;
        }
        
        if (typeof executionOf === "function") {
            if (midPosition === NapMidExecLapse.always || midPosition === NapMidExecLapse.onStart) this.middlewaresBeforeConn = this.middlewaresBeforeConn.concat([executionOf]);
            if (midPosition === NapMidExecLapse.always || midPosition === NapMidExecLapse.onEnd) this.middlewareAfterConn = this.middlewareAfterConn.concat([executionOf]);
            return this;
        }
        
        throw new Error("method '.with(..)': Invalid input");
    }

    useAuth(authorization: string): NAPConnectionModel {
        this.connAuthorization = authorization;
        return this;
    }
}

@Injectable()
export class NAPConnectionService {

    protected RURN: string = "/noapi/resources";
    protected AURN: string = "/noapi/auth";
    protected HOST: string = "";
    protected _AUTH_URL: string;
    protected _RESOURCE_URL: string;

    middlewaresBeforeProfile: NAPMiddlewaresProfiles = {};
    middlewaresAfterProfile: NAPMiddlewaresProfiles = {};
    defaultProfile: string = null;

    xNapPermissionOrder: Array<string> = [];
    

    private _authenticated: boolean = false;
    set authenticated(auth: boolean) {
        if (auth === this._authenticated) return;
        this._authenticated = auth;
        this.authenticatedChange.next(this._authenticated);
    }
    get authenticated(): boolean {
        return this._authenticated;
    }
    authenticatedChange: Subject<boolean> = new Subject();

    defaultAuthorization: string;

    constructor(private http: HttpClient) {
        this.calcuteURLs();
    }


    createBeforeMiddlewareProfile(profile: string, middlewares: Array<NAPMiddleware>) {
        this.middlewaresBeforeProfile[profile] = middlewares;
    }
    createAfterMiddlewareProfile(profile: string, middlewares: Array<NAPMiddleware>) {
        this.middlewaresAfterProfile[profile] = middlewares;
    }
    dropMiddlewareProfile(profile: string) {
        this.middlewaresBeforeProfile[profile] = null;
        this.middlewaresAfterProfile[profile] = null;
    }


    makeRequest(URL: string, reqBody: Array<any> | object): Promise<NapResponse<any>> {
        const responseObservable = this.http.post<NapResponse<any>>(URL, reqBody, {
            headers: {
                "x-nap-permission-order": this.xNapPermissionOrder.join(",")
            }
        });
        const prom = responseObservable.toPromise();
        prom.then((response) => {
            response.auth = !!response.auth;
            if (this.authenticated !== response.auth) {
                this.authenticated = response.auth;
            }
        });
        return prom;
    }

    private calcuteURLs() {
        this._AUTH_URL = this.HOST + this.AURN;
        this._RESOURCE_URL = this.HOST + this.RURN;
    }

    setResourceURN(path: string) {
        if (path[0] !== "/") path = "/" + path;
        this.RURN = path;
        this.calcuteURLs();
    }
    setAuthURN(path: string) {
        if (path[0] !== "/") path = "/" + path;
        this.AURN = path;
        this.calcuteURLs();
    }
    setHost(host: string) {
        if (host[host.length-1] === "/") host = host.substr(0, host.length-1);
        this.HOST = host;
        this.calcuteURLs();
    }

    makeResourceRequest(reqBody: Array<any> | object) {
        return this.makeRequest(this._RESOURCE_URL, reqBody);
    }
    makeAuthRequest(reqBody: Array<any> | object) {
        return this.makeRequest(this._AUTH_URL, reqBody);
    }
    
    getDefaultConnection(): NAPConnectionModel {
        const connInstance = new NAPConnectionModel(this, this.defaultAuthorization);
        connInstance.middlewaresBeforeConn = this.middlewaresBeforeProfile[this.defaultProfile] || [];
        connInstance.middlewareAfterConn = this.middlewaresAfterProfile[this.defaultProfile] || [];
        return connInstance;
    }

    ///////
    ///////  RESOURCES
    ///////
    list<T>(resource: string, filter?: Array<any>, options?: NAPListOptions): Promise<NapResponse<Array<T>>> {
        const connInstance = this.getDefaultConnection();
        return connInstance.list<T>(resource, filter, options);
    }
    read<T>(resource: string, filter?: Array<any>, options?: NAPReadOptions): Promise<NapResponse<Array<T>>> {
        const connInstance = this.getDefaultConnection();
        return connInstance.read<T>(resource, filter, options);
    }
    insert<T>(resource: string, data: T, options?: NAPInsertOptions): Promise<NapResponse<T>> {
        const connInstance = this.getDefaultConnection();
        return connInstance.insert<T>(resource, data, options);
    }
    update<T>(resource: string, data: T, filter: Array<any>, options?: NAPUpdateOptions): Promise<NapResponse<T>> {
        const connInstance = this.getDefaultConnection();
        return connInstance.update<T>(resource, data, filter, options);
    }
    delete<T>(resource: string, filter: Array<any>, options?: NAPDeleteOptions): Promise<NapResponse<T>> {
        const connInstance = this.getDefaultConnection();
        return connInstance.delete<T>(resource, filter, options);
    }
    aggregate<T>(resource: string, aggregation: NapAggregation, filter?: Array<any>, options?: NAPAggregateOptions): Promise<NapResponse<T>> {
        const connInstance = this.getDefaultConnection();
        return connInstance.aggregate<T>(resource, aggregation, filter, options);
    }

    with(executionOf: string | NAPMiddleware | Array<NAPMiddleware>, midPosition: NapMidExecLapse = NapMidExecLapse.always) {
        const connInstance = this.getDefaultConnection();
        return connInstance.with(executionOf, midPosition);
    }
    
}
