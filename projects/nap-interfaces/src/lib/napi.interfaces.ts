export interface NapResponse<T> {
    status: "success" | "error";
    statusCode: number;
    data?: T;
    auth?: boolean;
    resourcesAllowed?: any;
}

export interface NapAggregation {
    params: Array<Array<string>>;
    fields: Array<string>;
    group?: Array<string>;
}


////
////// PERFIS DE REQUISICOES
////

export type NAPMiddleware = (o: any) => any;

export interface NAPMiddlewaresProfiles {
    [key: string]: Array<NAPMiddleware>;
}

////
/////// OPCOES PARA REQUISICAO
////

export interface NAPLoginOptions {
    sessionEngine?: string;
    avoidAuth?: boolean;
}
export interface NAPLoginIdentities {
    [key: string]: string | number;
}
export interface NAPResetPasswordOptions {
    destUrl?: string;
}

//// OPCOES DE OPERACOES
export interface NAPPayloadI {
    [key: string]: any;
}
export interface NAPGroupOption {
    selector: string;
    desc: string;
    isExpanded: boolean;
}
export interface NAPSortOption {
    selector: string;
    desc: boolean;
}
export interface NAPProjectionOption {
    [key: string]: any;
}

export interface NAPListOptions {
    payload?: NAPPayloadI;
    group?: Array<NAPGroupOption>;
    sort?: Array<NAPSortOption>;
    skip?: number;
    take?: number;
    projection?: NAPProjectionOption;
    categories?: Array<string>;
}
export interface NAPReadOptions {
    payload?: NAPPayloadI;
}
export interface NAPInsertOptions {
    payload?: NAPPayloadI;
}
export interface NAPUpdateOptions {
    payload?: NAPPayloadI;
    unique?: boolean;
}
export interface NAPDeleteOptions {
    payload?: NAPPayloadI;
}
export interface NAPAggregateOptions extends NAPListOptions {
}

////
////// RECURSOS PERMITIDOS
////

export interface NapResAllowedWriteI {
    name: string;
    type?: string;
    required?: boolean;
    default?: any;
    choices?: Array<string | number>;
    label?: string;
    description?: string;
}

export interface NapResAllowedReadI {
    name: string;
    type?: string;
    label?: string;
    description?: string;
}

export interface NapResOperationsAllowedI {
    read?: Array<string | NapResAllowedReadI>;
    insert?: Array<string | NapResAllowedWriteI>;
    update?: Array<string | NapResAllowedWriteI>;
    delete?: boolean;
}

export interface NapResourcesAllowedI {
    [key: string]: NapResOperationsAllowedI;
}


// 
//
export type NAPFunctionFilter = () => Array<any>;