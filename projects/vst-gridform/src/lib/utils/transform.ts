
export function capitalize(s) {
    return s && s[0].toUpperCase() + s.slice(1);
}

export function titleize(s) {
    var string_array = s.split(" ");
    string_array = string_array.map(function (str) {
       return capitalize(str); 
    });
    return string_array.join(" ");
}

function realtransformField(fieldName: Array<string>, data: any, use: any) {
    if (!data) return;
    const field: string = fieldName.shift();
    if (fieldName.length === 0) {
        if (data[field] === undefined) return;
        if (use === "capitalize" && typeof data[field] === "string") {
            data[field] = capitalize(data[field]);
        } else if (use === "titleize" && typeof data[field] === "string") {
            data[field] = titleize(data[field]);
        } else if (use === "uppercase" && typeof data[field] === "string") {
            data[field] = data[field].toUpperCase();
        } else if (use === "lowercase" && typeof data[field] === "string") {
            data[field] = data[field].toLowerCase();
        } else if (typeof use === "function") {
            data[field] = use(data[field]);
        }
    } else {
        if (typeof data[field] === "object") realtransformField(fieldName, data[field], use);
    }
}

export function transformField(fieldName: string, data: any, use: any) {
    realtransformField(fieldName.split('.'), data, use);
}