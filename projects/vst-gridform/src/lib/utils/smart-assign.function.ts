import { smartDeepCopy } from './smart-copy.function';

export function smartAssign(target: object, source: object) {
    Object.keys(source).forEach((sourceField) => {
        if (typeof target[sourceField] === "object" && typeof source[sourceField] === "object") {
            smartAssign(target[sourceField], source[sourceField]);
        } else if (target[sourceField] == undefined) target[sourceField] = smartDeepCopy(source[sourceField])
    })
}