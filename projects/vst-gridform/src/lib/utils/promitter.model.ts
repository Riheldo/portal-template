import { Subscription, Observable, Subscriber, Observer, Subject, PartialObserver } from 'rxjs';

export class VsPromitter<T=any> {

    private resolved: boolean = false;
    private _externalSubscription: Subscription = new Subscription();
    private _subs: Subscription = new Subscription();
    private _subj: Subject<T> = new Subject();
    private _obs: Observable<T> = new Observable((obsSub: Subscriber<T>) => {
        this._subj.subscribe((v) => {
            obsSub.next(v);
        });
    });

    subscribe(observer: PartialObserver<T> | ((val: T) => void), skipPromitter: boolean = false): Subscription {
        if (typeof observer === "function") {
            observer = {next: observer};
        }
        const exSub = this._obs.subscribe(observer);
        this._externalSubscription.add(exSub);
        if (!skipPromitter) this.checkResolver();

        return exSub;
    }

    emit(value: T) {
        this._subj.next(value);
        this.resolved = true;
    }

    unsubscribeAll() {
        this._externalSubscription.unsubscribe();
    }

    finish() {
        this._subs.unsubscribe();
    }

    checkResolver() {
        if (this.resolved) {
            this._subj.next();
        }
    }

    resetResolve() {
        this.resolved = false;
    }

    resetResolveAction() {
        return () => {
            this.resetResolve();
        }
    }

}