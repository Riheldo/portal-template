import { VSTFormSchema } from '../vst-form/interfaces/vst-form-schema.interface';

let VST_GLOBAL_FORM_CONFIG: VSTFormSchema = {}

export function VstGlobalFormConfig(): VSTFormSchema;
export function VstGlobalFormConfig(conf: VSTFormSchema);
export function VstGlobalFormConfig(conf?: VSTFormSchema) {
    if (!conf) return VST_GLOBAL_FORM_CONFIG;
    VST_GLOBAL_FORM_CONFIG = conf;
}
