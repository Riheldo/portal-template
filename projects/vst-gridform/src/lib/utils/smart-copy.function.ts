export function smartDeepCopy(data: any): any {
    if (typeof data === "bigint") return data;
    if (typeof data === "boolean") return data;
    if (typeof data === "number") return data;
    if (typeof data === "string") return data;
    if (typeof data === "undefined") return undefined;
    if (typeof data === "function") return data;
    if (typeof data === "symbol") return data;
    if (typeof data === null) return data;

    // codigo que realmente fara diferenca
    // data eh object
    if (data instanceof Array) {
        return data.map((item) => smartDeepCopy(item));
    } else if (data.__proto__ === Object.prototype) {
        // objeto comum
        const aux = {};
        Object.keys(data).forEach((dataKey) => {
            aux[dataKey] = smartDeepCopy(data[dataKey]);
        });
        return aux;
    }

    return data; // objeto, tipo instancia de classe etc
}