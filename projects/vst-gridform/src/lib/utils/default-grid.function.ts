import { VSTGridSchema } from '../vst-grid/interfaces/vst-grid-schema.interface';

let VST_GLOBAL_GRID_CONFIG: VSTGridSchema = {}

export function VstGlobalGridConfig(): VSTGridSchema;
export function VstGlobalGridConfig(conf: VSTGridSchema);
export function VstGlobalGridConfig(conf?: VSTGridSchema) {
    if (!conf) return VST_GLOBAL_GRID_CONFIG;
    VST_GLOBAL_GRID_CONFIG = conf;
}
