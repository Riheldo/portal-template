import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VSTGridComponent } from './vst-grid.component';
import { VSTFormModule } from '../vst-form/vst-form.module';
import { DxDataGridModule } from 'devextreme-angular/ui/data-grid';
import { DxButtonModule } from 'devextreme-angular/ui/button';
import { DxPopupModule } from 'devextreme-angular/ui/popup';
import { VstDisplayHiddenColumnsComponent } from './components/vst-display-hidden-columns/vst-display-hidden-columns.component';

@NgModule({
    imports: [
        CommonModule,
        VSTFormModule,
        DxDataGridModule,
        DxButtonModule,
        DxPopupModule
    ],
    declarations: [
        VSTGridComponent,
        VstDisplayHiddenColumnsComponent
    ],
    exports: [
        VSTGridComponent
    ]
})
export class VSTGridModule { }
