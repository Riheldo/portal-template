import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { VstDisplayField } from '../../vst-grid.component';
import { DxDataGridComponent } from 'devextreme-angular/ui/data-grid';

@Component({
  selector: 'vst-display-hidden-columns',
  templateUrl: './vst-display-hidden-columns.component.html',
  styleUrls: ['./vst-display-hidden-columns.component.scss']
})
export class VstDisplayHiddenColumnsComponent {

    @ViewChild(DxDataGridComponent, {static: false}) dataGrid: DxDataGridComponent;

    @Input() displayFields: Array<VstDisplayField> = [];
    @Input() displayData: any = {};

    public table: Array<{key: string, value: string}> = [];

    initDataGrid() {
        this.dataGrid.instance.option('columns', [
            {dataField: "key"},
            {dataField: "value"},
        ]);
        this.dataGrid.instance.option('showColumnHeaders', false);
        this.dataGrid.instance.option('showColumnLines', true);
        this.dataGrid.instance.option('showRowLines', true);
        this.dataGrid.instance.option('showBorders', true);
        this.setup();
    }

    setup() {
        this.table = [];
        this.displayFields.forEach((item) => {
            if (this.displayData[item.key] == undefined) return;
            this.table.push({key: item.text, value: this.displayData[item.key]});
        });
    }
}
