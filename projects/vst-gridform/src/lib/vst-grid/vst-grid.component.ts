////// ANGULAR
import { Component, ViewChild, ElementRef, Input, EventEmitter, Output } from '@angular/core';

////// DEVEXTREME
import DevExpress from 'devextreme/bundles/dx.all';
import { DxDataGridComponent } from 'devextreme-angular/ui/data-grid';
import validationEngine from 'devextreme/ui/validation_engine';

////// INTERNS
import { VSTGridSchema, VstGridBaseEditing, VstFormGridButton, VstFormGridCancelButton } from './interfaces/vst-grid-schema.interface';
import { VSTdxFormComponent } from '../vst-form/vst-form.component';
import { VSTFormItems, VSTFormItem, CustomTemplateFormat } from '../vst-form/interfaces/vst-customtemplate';
import { VSTFormSchema } from '../vst-form/interfaces/vst-form-schema.interface';
import { VstCommomSchemaInitializeComponent } from '../vst-commom-initialize/vst-commom-initialize.component';
import { uuid4 } from '../utils/uuid';
import { VstGlobalGridConfig } from '../utils/default-grid.function';
import { smartAssign } from '../utils/smart-assign.function';
import { DxPopupComponent } from 'devextreme-angular/ui/popup';

export interface TemplateI {
    name: string;
    template: ElementRef;
}

export interface FormError {
    title: string;
    text: string;
}

export enum VstGridEvent {
    gridHidden,
    gridBack,
    gridInitialized,
}

export interface VstDisplayField {
    key: string;
    text: string;
}

@Component({
    selector: 'vst-grid',
    templateUrl: './vst-grid.component.html',
    styleUrls: ['./vst-grid.component.scss']
})
export class VSTGridComponent<T = any> extends VstCommomSchemaInitializeComponent<VSTGridSchema, DxDataGridComponent> {

    @ViewChild(DxDataGridComponent, {static: false}) dxInstance: DxDataGridComponent;
    @Input() templates: Array<TemplateI>;
    executionsBeforeInitialize: Array<()=>void> = [];
    
    @ViewChild(VSTdxFormComponent, {static: false}) vstForm: VSTdxFormComponent<T>;
    externFormEditing: boolean = false;
    formSchema: VSTFormSchema;
    loadingDataOnForm: Promise<T>;
    dataEditing: T;
    dataEditingKey: object;
    dataEditingType: "insert" | "update" | "view";
    format: CustomTemplateFormat = 'edit';
    protected originalFormat: CustomTemplateFormat;
    
    formError: FormError;
    saveLoading: boolean = false;
    useBorder: boolean = false;
    useForm: boolean = true;

    hiddenFields: Array<VstDisplayField> = [];

    @Output() onEvent: EventEmitter<VstGridEvent> = new EventEmitter();

    @ViewChild('popUpConfirmation', {static: false}) popUpConfirmation: DxPopupComponent;

    get cancelEditing(): VstFormGridCancelButton {
        if (this.schema && this.schema.editing && this.schema.editing.cancelButton) return this.schema.editing.cancelButton;
        return {};
    }
    get saveEditing(): VstFormGridButton {
        if (this.schema && this.schema.editing && this.schema.editing.saveButton) return this.schema.editing.saveButton;
        return {};
    }
    get backViewBtn(): VstFormGridButton {
        if (this.schema && this.schema.editing && this.schema.editing.backButton) return this.schema.editing.backButton;
        return {};
    }
    get formOperation(): "create" | "read" | "update" {
        if (this.dataEditingType === "view") return "read";
        if (this.dataEditingType === "insert") return "create";
        if (this.dataEditingType === "update") return "update";
    }


    // reference grid datasource
    private gridDataSource: DevExpress.data.DataSource | DevExpress.data.DataSourceOptions | string | Array<any>;

    constructor() {
        super();
        this.formatChange.subscribe(() => {
            this.handleFormat();
        })
    }

    beforeInitialize() {
        super.beforeInitialize();
        if (this.schema.rowAlternationEnabled==undefined) this.schema.rowAlternationEnabled = true;
        // get default params to datagrid
        const defaultParams = VstGlobalGridConfig();
        smartAssign(this.schema, defaultParams);
    }

    viewerColumnAlreadyInserted: boolean = false;
    customInitialize(): void {
        this.schema.columns.forEach((schemaColumn) => {
            /// simple item
            this.insertCssInNode(schemaColumn);
        })


        this.resolveTasks();

        this.dxInstance.onToolbarPreparing.subscribe((e) => {
            const alreadyHasRefresh: boolean = !!(e.toolbarOptions.items as Array<any>).filter((item) => item.widget=='dxButton' && item.options && item.options.icon == 'refresh').length;
            if ((this.schema.allowRefresh === undefined || this.schema.allowRefresh) && !alreadyHasRefresh) {
                e.toolbarOptions.items.unshift({
                    location: 'after',
                    widget: 'dxButton',
                    options: {
                        icon: 'refresh',
                        onClick: () => {
                            this.dxInstance.instance.refresh();
                        }
                    }
                });
            }
            for (const item of e.toolbarOptions.items) {
                if (item.name == "addRowButton") {
                    let originalHandlerClick = item.options.onClick;
                    item.options.onClick = (e) => {
                        if (this.willGridHandlerEdition()) {
                            originalHandlerClick();
                            return;
                        }
                        this.openExternEdition("insert");
                    }
                }
            }
        });

        this.dxInstance.onEditingStart.subscribe((e) => {
            if (!this.willGridHandlerEdition()) {
                e.cancel = true;
                this.openExternEdition("update", e.data, e.key);
            }
        });

        this.dxInstance.onContentReady.subscribe((e) => {
            const grid: DevExpress.ui.dxDataGrid = e.component as any;
            this.hiddenFields = [];
            this.dxInstance.columns.forEach((columnItem) => {
                if (typeof columnItem === "string") columnItem = {dataField: columnItem};
                if (!columnItem.dataField) return;
                let notFound = true;
                grid.getVisibleColumns().forEach((visibleColum) => {
                    if (typeof visibleColum === "string") visibleColum = {dataField: visibleColum};
                    if (!visibleColum.dataField) return;
                    if ((columnItem as any).dataField === visibleColum.dataField) {
                        notFound = false;
                        return;
                    }
                });
                if (notFound) this.hiddenFields.push({key: columnItem.dataField, text: columnItem.caption || columnItem.dataField});
            });
            const masterDetail = grid.option("masterDetail");
            if (masterDetail && masterDetail.showHiddenColumns) {
                if (this.hiddenFields.length) {
                    if (!masterDetail.enabled) {
                        grid.beginUpdate();
                        grid.option("masterDetail.enabled", true);
                        grid.option("masterDetail.template", 'showHiddenColumnsTemplate');
                        grid.endUpdate();
                    }
                } else {
                    if (masterDetail.enabled) {
                        grid.beginUpdate();
                        grid.option("masterDetail.enabled", false);
                        grid.option("masterDetail.template", 'showHiddenColumnsTemplate');
                        grid.endUpdate();
                    }
                }
            }
        });
        
        // forcar asyncronia
        setTimeout(() => {
            if (this.schema.editing && this.schema.editing.allowViewing && !this.willGridHandlerEdition() && !this.viewerColumnAlreadyInserted) {
                this.dxInstance.columns.push({
                    showInColumnChooser: false,
                    allowHiding: false,
                    allowEditing: false,
                    allowExporting: false,
                    allowFiltering: false,
                    allowFixing: false,
                    allowGrouping: false,
                    allowHeaderFiltering: false,
                    allowReordering: false,
                    allowResizing: false,
                    allowSearch: false,
                    allowSorting: false,
                    type: "buttons",
                    buttons: [{
                        hint: "Visualizar",
                        text: "Visualizar",
                        icon: "find",
                        onClick: (e) => {
                            try {
                                this.openExternEdition("view", e.row.data, e.row.key);
                            } catch (error) {
                                console.error(error);
                            }
                            return false;
                        }
                    }, "edit", "delete"]
                })
                this.viewerColumnAlreadyInserted = true;
            }
        });
        
        
        this.dxInstance.onRowDblClick.subscribe((e) => {
            const masterDetail = this.dxInstance.instance.option("masterDetail");
            if (masterDetail.enabled) {
                if (e.isExpanded) {
                    this.dxInstance.instance.collapseRow(e.rowIndex);
                } else {
                    this.dxInstance.instance.expandRow(e.rowIndex);
                }
            } 
        });


        this.referenceGridDataSource();
        this.dxInstance.dataSourceChange.subscribe((e) => {
            this.referenceGridDataSource();
        });

        if (this.schema && this.schema.editing && this.schema.editing.useBorder) {
            this.useBorder = true;
        }

        this.onEvent.emit(VstGridEvent.gridInitialized);

        this.originalFormat = this.format;
        setTimeout(() => {
            this.handleFormat();
        });
    }

    referenceGridDataSource() {
        this.gridDataSource = this.dxInstance.dataSource;
    }


    willGridHandlerEdition() {
        if (this.dxInstance && this.dxInstance.editing && this.dxInstance.editing.form && this.dxInstance.editing.form.items) {
            if (this.schema && this.schema.editing && (this.schema.editing as VstGridBaseEditing).forceFormHandler) return false;
            if (!this.walkOnSchemaForm(this.dxInstance.editing.form.items as VSTFormItems)) {
                return false;
            }
        }
        return true;
    }

    walkOnSchemaForm(item: VSTFormItem | VSTFormItems) {
        if (!item) return true;
        if (item instanceof Array) {
            for (const subItem of item) {
                if (!this.walkOnSchemaForm(subItem)) return false;
            }
        } else if (item.itemType === "simple" || item.itemType === undefined) {
            if (
                (item as DevExpress.ui.dxFormSimpleItem).editorType && 
                (((item as DevExpress.ui.dxFormSimpleItem).editorType.substr(0, 2) !== "dx") || 
                (
                    (item as DevExpress.ui.dxFormSimpleItem).editorType !== "dxTextBox" &&
                    (item as DevExpress.ui.dxFormSimpleItem).editorType !== "dxDateBox" &&
                    (item as DevExpress.ui.dxFormSimpleItem).editorType !== "dxCheckBox" &&
                    (item as DevExpress.ui.dxFormSimpleItem).editorType !== "dxNumberBox"
                ))
            ) {
                return false;
            }
        } else if (item.itemType === "group" && item.items) {
            return this.walkOnSchemaForm(item.items as VSTFormItems);
        }
        return true;
    }

    getFormSchema(): VSTFormSchema {
        const auxFormSchema: VSTFormSchema = {};
        Object.assign(auxFormSchema, this.dxInstance.editing.form);
        auxFormSchema["colCount"] = 1;
        if (!auxFormSchema["labelLocation"]) {
            if (this.schema.editing && this.schema.editing.form && this.schema.editing.form.labelLocation) {
                auxFormSchema["labelLocation"] = this.schema.editing.form.labelLocation;
            }
        }
        if (this.schema.transform) auxFormSchema.transform = this.schema.transform;
        return auxFormSchema;
    }

    openExternEdition(typeOperation: "insert" | "update" | "view", data?: T, key?: object) {
        this.formSchema = this.getFormSchema();
        if (typeOperation === 'view') this.formSchema['readOnly'] = true;

        // verificar se abre popup ou nao
        if (this.dxInstance.editing && this.dxInstance.editing.mode === "popup") {
            this.useForm = false;
        } else {
            this.useForm = true;
        }
        const instanceDataSource = this.dxInstance.dataSource;
        if ((typeOperation === 'update' || typeOperation === 'view') && typeof instanceDataSource !== "string" && !(instanceDataSource instanceof Array)) {
            let store: DevExpress.data.Store;
            if (typeof (instanceDataSource as DevExpress.data.DataSource).store === "function") {
                store = (instanceDataSource as DevExpress.data.DataSource).store();
            } else if (typeof (instanceDataSource as DevExpress.data.Store).insert === "function" && typeof (instanceDataSource as DevExpress.data.Store).update === "function") {
                store = instanceDataSource as DevExpress.data.Store;
            }
            if (typeof (store as any)._byKeyFunc === "function") {
                this.loadingDataOnForm = new Promise((resolveData) => {
                    store.byKey(key).then((resData) => {
                        if (resData) this.dataEditing = resData;
                        resolveData(this.dataEditing);
                    });
                });
            } else if (data) this.loadingDataOnForm = Promise.resolve(data);
        }
        
        this.format = (typeOperation === 'view') ? 'readonly' : 'edit';
        this.externFormEditing = true;
        this.onEvent.emit(VstGridEvent.gridHidden);
        this.instanceInitialized = false;
        this.dataEditingKey = key;
        this.dataEditingType = typeOperation;
        if (typeOperation === "insert") {
            this.dataEditing = {} as T;
        } else {
            this.dataEditing = data;
        }

        // atualizar a formatacao dos componentes customizados
        const cbFormReady = this.formSchema.onInitialized;
        this.formSchema.onInitialized = (...e) => {
            setTimeout(() => {
                this.handleFormat();
            });
            if (typeof cbFormReady === "function") cbFormReady(...e);
        }
        
    }

    stopExternalEditing(): Promise<void> {
        this.vstForm.resetPartialData();
        this.dataEditing = {} as T;
        this.externFormEditing = false;
        this.onEvent.emit(VstGridEvent.gridBack);
        this.formError = null;
        this.saveLoading = false;
        this.format = this.originalFormat;
        this.loadingDataOnForm = null;
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve();
            });
        });
    }

    changeEditingMode(to: "insert" | "update" | "view", data?: T, key?: object) {
        if (this.dataEditingType === to) return;
        this.dataEditingType = to;
        if (to === "insert") {
            this.format = 'edit';
            this.dataEditingKey = undefined;
            this.dataEditing = {} as T;
        } else if (to === "update") {
            this.format = 'edit';
            if (!key) throw new Error("Falta passar o parametro key para mudar modo de edicao");
            this.dataEditingKey = key;
            if (!data) throw new Error("Falta passar o parametro data para mudar modo de edicao");
            this.dataEditing = data;
        } else if (to === "view") {
            this.format = 'readonly';
        }
    }

    
    save() {
        try {
            // validacao formulario
            if (this.vstForm.validationGroupName && validationEngine) {
                const groupConfig = validationEngine.getGroupConfig(this.vstForm.validationGroupName);
                if (groupConfig && groupConfig.validators && groupConfig.validators.length) {
                    const validationResult: DevExpress.ui.dxValidationGroupResult = validationEngine.validateGroup(this.vstForm.validationGroupName);
                    if(!validationResult.isValid) return false;
                }
            }
    
            this.saveLoading = true;
    
            // logica
            const editedData: T = this.dataEditing;
            // transform fields before save
            this.dataTrasformation(editedData);

            const dataSource = this.gridDataSource;
            
            // POSSIVEIS TIPOS DO DATASOURCE
            // const d: DevExpress.data.DataSource | DevExpress.data.DataSourceOptions | string | Array<any>

            let cancel = false;
            if (this.dataEditingType === "insert") {
                /** A function that is executed before a new row is inserted into the data source. */
                if (this.schema && typeof this.schema.onRowInserting === "function") {
                    const onRowInserting: { component?: any, element?: DevExpress.core.dxElement, data?: any, cancel?: boolean | Promise<void> | JQueryPromise<void> } = {
                        component: null,
                        element: null,
                        data: this.dataEditing,
                        cancel: false,
                    }
                    this.schema.onRowInserting(onRowInserting);
                    if (onRowInserting.cancel) cancel = true;
                }

            } else if (this.dataEditingType === "update") {
                /** A function that is executed before a row is updated in the data source. */
                if (this.schema && typeof this.schema.onRowUpdating === "function") {
                    const onRowUpdating: { component?: any, element?: DevExpress.core.dxElement, oldData?: any, newData?: any, key?: any, cancel?: boolean | Promise<void> | JQueryPromise<void> } = {
                        component: null,
                        element: null,
                        oldData: null,
                        newData: this.dataEditing,
                        key: this.dataEditingKey,
                        cancel: false
                    };
                    this.schema.onRowUpdating(onRowUpdating);
                    if (onRowUpdating.cancel) cancel = true;
                }
            }
            
            if (cancel) {
                this.saveLoading = false;
                return;
            }
    
            if (dataSource instanceof Array) {
                if (!editedData['__KEY__']) {
                    editedData['__KEY__'] = uuid4();
                    dataSource.push(editedData);
                } else {
                    for (let i = 0; i < dataSource.length; i++) {
                        if (dataSource[i]['__KEY__'] === editedData['__KEY__']) {
                            dataSource[i] = editedData;
                            break;
                        }
                    }
                }
                this.stopExternalEditing();
            } else if (dataSource) {
                let store: DevExpress.data.Store;
                if (typeof (dataSource as DevExpress.data.DataSource).store === "function") {
                    store = (dataSource as DevExpress.data.DataSource).store();
                } else if (typeof (dataSource as DevExpress.data.Store).insert === "function" && typeof (dataSource as DevExpress.data.Store).update === "function") {
                    store = dataSource as DevExpress.data.Store;
                } else {
                    return this.accuseError(new Error("DataSource nao eh valida!"));
                }
    
                // store certificada
                if (this.dataEditingType === "insert") {
                    store.insert(editedData).then((res) => {
                        this.stopExternalEditing().then(() => {
                            if (this.dxInstance) {
                                /** A function that is executed after a new row has been inserted into the data source. */
                                const onRowInserted: { component?: any, element?: DevExpress.core.dxElement, data?: any, key?: any, error?: Error } = {
                                    component: this.dxInstance,
                                    element: this.dxInstance.instance.element(),
                                    data: res,
                                    key: this.dataEditingKey,
                                }
                                this.dxInstance.onRowInserted.emit(onRowInserted);

                                // refresh to list data table
                                this.dxInstance.instance.refresh();
                            }
                        })
                    }).catch((reason) => {
                        this.accuseError(reason);
                    });
                } else {
                    store.update(this.dataEditingKey, editedData).then((res) => {
                        this.stopExternalEditing().then(() => {
                            if (this.dxInstance) {
                                /** A function that is executed after a row has been updated in the data source. */
                                const onRowUpdated: { component?: any, element?: DevExpress.core.dxElement, data?: any, key?: any, error?: Error } = {
                                    component: this.dxInstance,
                                    element: this.dxInstance.instance.element(),
                                    data: res,
                                    key: this.dataEditingKey,
                                };
                                this.dxInstance.onRowUpdated.emit(onRowUpdated);

                                // refresh to list data table
                                this.dxInstance.instance.refresh();
                            }
                        })
                    }).catch((reason) => {
                        this.accuseError(reason);
                    });
                }
            }
        } catch (error) {
            this.accuseError(error);
        }

        return false;
    }

    accuseError(err: Error | string): false {
        this.saveLoading = false;
        let errTitle = "Erro", errText = "";
        if (typeof err === "string") {
            errText = err;
        } else {
            if (err.name && err.name !== "Error") errTitle = err.name;
            errText = err.message;
        }
        this.formError = {title: errTitle, text: errText};
        return false;
    }
    
    resolveTasks() {
        if (this.executionsBeforeInitialize && this.executionsBeforeInitialize.length > 0) {
            this.executionsBeforeInitialize.forEach((func) => {
                func();
            });
            this.executionsBeforeInitialize = [];
        }
    }
    
    addTask(func: () => void) {
        if (!this.executionsBeforeInitialize) this.executionsBeforeInitialize = [];
        this.executionsBeforeInitialize.push(func);
    }

    private async askToLeaveForm(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.popUpConfirmation.visible = true;
            const el = document.createElement('p');
            el.style.textAlign = "center";
            el.style.fontWeight = "bold";
            el.textContent = this.cancelEditing.messageToLeave || "Você tem certeza que deseja Sair?";
            this.popUpConfirmation.contentTemplate = el;
            this.popUpConfirmation.closeOnOutsideClick = false;
            this.popUpConfirmation.title = "Confirmação de Operação";
            this.popUpConfirmation.toolbarItems = [
                {widget: 'dxButton', location: 'center', toolbar: 'bottom', options: {
                    type: 'normal',
                    text: 'Cancelar',
                    onClick: () => {
                        this.popUpConfirmation.visible = false;
                        resolve(false);
                    }
                }},
                {widget: 'dxButton', location: 'center', toolbar: 'bottom', options: {
                    type: 'danger',
                    text: 'Sair',
                    onClick: () => {
                        this.popUpConfirmation.visible = false;
                        resolve(true);
                    }
                }}
            ];
        });
    }
    async leaveForm() {
        let leaveForm: boolean = true;
        if (this.cancelEditing.askBeforeLeave === true) {
            leaveForm = await this.askToLeaveForm(); 
        } else if (typeof this.cancelEditing.askBeforeLeave === 'function') {
            let result: boolean | Promise<boolean> = this.cancelEditing.askBeforeLeave();
            if (typeof result !== 'boolean') {
                result = await result;
            }
            if (result) leaveForm = await this.askToLeaveForm();
        }
        
        if (leaveForm) {
            if (typeof this.cancelEditing.onClick === "function") {
                let result = this.cancelEditing.onClick();
                if (typeof result !== "boolean") {
                    result.then((v) => {
                        result = v;
                        if (result) this.stopExternalEditing(); 
                    })
                } else if (result) {
                    this.stopExternalEditing();
                }
            } else {
                this.stopExternalEditing();
            }
        }
    }
    _btnCancelClick() {
        this.leaveForm();
    }
    _btnSaveClick() {
        if (typeof this.saveEditing.onClick === "function") {
            return this.saveEditing.onClick();
        }
    }
    _btnBackClick() {
        if (typeof this.backViewBtn.onClick === "function") {
            let result = this.backViewBtn.onClick();
            if (typeof result !== "boolean") {
                result.then((v) => {
                    result = v;
                    if (result) this.stopExternalEditing(); 
                })
            } else if (result) {
                this.stopExternalEditing();
            }
        } else {
            this.stopExternalEditing();
        }
    }

    handleFormat() {
        if (this.format === 'edit' || this.format == undefined) {
            if (this.dxInstance) {
                const alAdd = (this.schema.editing && this.schema.editing.allowAdding != undefined) ? this.schema.editing.allowAdding : false;
                const alUpd = (this.schema.editing && this.schema.editing.allowUpdating != undefined) ? this.schema.editing.allowAdding : false;
                const alDel = (this.schema.editing && this.schema.editing.allowDeleting != undefined) ? this.schema.editing.allowAdding : false;
                this.dxInstance.editing.allowAdding = alAdd;
                this.dxInstance.editing.allowUpdating = alUpd;
                this.dxInstance.editing.allowDeleting = alDel;
            }
        } else if (this.dxInstance) {
            this.dxInstance.editing.allowAdding = false;
            this.dxInstance.editing.allowUpdating = false;
            this.dxInstance.editing.allowDeleting = false;
        }
        if (this.vstForm) {
            this.vstForm.format = this.format;
            this.vstForm.formatChange.emit(this.format);
        }
    }

}
