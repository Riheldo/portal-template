import DevExpress from "devextreme/bundles/dx.all";
import { VSTFormSchema } from "../../vst-form/interfaces/vst-form-schema.interface";
import { VstDxGrid } from './vst-dxgrid.interface';
import { VsTransform } from '../../vst-form/interfaces/vst-transform.interface';

export interface VstFormGridButton {
    disabled?: boolean;
    elementAttr?: object;
    hint?: string;
    icon?: string;
    onClick?: () => boolean | Promise<boolean>;
    text?: string;
    type?: 'back' | 'danger' | 'default' | 'normal' | 'success';
}

export interface VstFormGridCancelButton extends VstFormGridButton {
    askBeforeLeave?: boolean | (() => boolean | Promise<boolean>);
    messageToLeave?: string;
}

export interface VstGridBaseEditing {
    /**
     * CONFIGURAÇÕES ADICIONAIS
     */
    forceFormHandler?: boolean;
    cancelButton?: VstFormGridCancelButton;
    saveButton?: VstFormGridButton;
    backButton?: VstFormGridButton;
    allowViewing?: boolean;

    /////// CONFIGS DEVEXTREME
    useBorder?: boolean;
    /** Specifies whether a user can add new rows. */
    allowAdding?: boolean;
    /** Specifies whether a user can delete rows. */
    allowDeleting?: boolean;
    /** Specifies whether a user can update rows. */
    allowUpdating?: boolean;
    /** Configures the form. Used only if editing.mode is "form" or "popup". */
    form?: VSTFormSchema;
    /** Specifies how a user edits data. */
    mode?: 'batch' | 'cell' | 'row' | 'form' | 'popup';
    /** Configures the popup. Used only if editing.mode is "popup". */
    popup?: DevExpress.ui.dxPopupOptions;
    /** Overriden. */
    texts?: DevExpress.ui.GridBaseEditingTexts;
    /** Specifies whether the editing column uses icons instead of links. */
    useIcons?: boolean;
}

// REWRITING DevExpress.ui.dxDataGridOptions 
export interface VSTGridSchema extends DevExpress.ui.GridBaseOptions<VstDxGrid> {
    /**
     * CONFIGURAÇÕES ADICIONAIS PARA GRID CUSTOMIZADA
     */
    allowRefresh?: boolean;
    // transformacoes de texto antes de salvar
    transform?: VsTransform;

    /**
     * CONFIG DO DEVEXTREME, PRECISAMENTE dxDataGridOptions
     */
    /** An array of grid columns. */
    columns?: Array<DevExpress.ui.dxDataGridColumn>;
    /** Specifies a function that customizes grid columns after they are created. */
    customizeColumns?: ((columns: Array<DevExpress.ui.dxDataGridColumn>) => any);
    /** Customizes data before exporting. */
    customizeExportData?: ((columns: Array<DevExpress.ui.dxDataGridColumn>, rows: Array<DevExpress.ui.dxDataGridRowObject>) => any);
    /** Configures editing. */
    editing?: VstGridBaseEditing & DevExpress.ui.GridBaseEditing;
    /** Configures client-side exporting. */
    export?: { enabled?: boolean, fileName?: string, excelFilterEnabled?: boolean, excelWrapTextEnabled?: boolean, proxyUrl?: string, allowExportSelectedData?: boolean, ignoreExcelErrors?: boolean, texts?: { exportTo?: string, exportAll?: string, exportSelectedRows?: string } };
    /** Configures grouping. */
    grouping?: { autoExpandAll?: boolean, allowCollapsing?: boolean, contextMenuEnabled?: boolean, expandMode?: 'buttonClick' | 'rowClick', texts?: { groupContinuesMessage?: string, groupContinuedMessage?: string, groupByThisColumn?: string, ungroup?: string, ungroupAll?: string } };
    /** Configures the group panel. */
    groupPanel?: { visible?: boolean | 'auto', emptyPanelText?: string, allowColumnDragging?: boolean };
    /** Specifies which data field provides keys for data items. Applies only if data is a simple array. */
    keyExpr?: string | Array<string>;
    /** Allows you to build a master-detail interface in the grid. */
    masterDetail?: { enabled?: boolean, autoExpandAll?: boolean, template?: DevExpress.ui.template | ((detailElement: DevExpress.core.dxElement, detailInfo: { key?: any, data?: any }) => any), showHiddenColumns?: boolean };
    /** A function that is executed when a user clicks a cell. */
    onCellClick?: ((e: { component?: DevExpress.ui.dxDataGrid, element?: DevExpress.core.dxElement, model?: any, jQueryEvent?: JQueryEventObject, event?: DevExpress.event, data?: any, key?: any, value?: any, displayValue?: string, text?: string, columnIndex?: number, column?: any, rowIndex?: number, rowType?: string, cellElement?: DevExpress.core.dxElement, row?: DevExpress.ui.dxDataGridRowObject }) => any) | string;
    /** A function that is executed after the pointer enters or leaves a cell. */
    onCellHoverChanged?: ((e: { component?: DevExpress.ui.dxDataGrid, element?: DevExpress.core.dxElement, model?: any, eventType?: string, data?: any, key?: any, value?: any, text?: string, displayValue?: string, columnIndex?: number, rowIndex?: number, column?: DevExpress.ui.dxDataGridColumn, rowType?: string, cellElement?: DevExpress.core.dxElement, row?: DevExpress.ui.dxDataGridRowObject }) => any);
    /** A function that is executed after the widget creates a cell. */
    onCellPrepared?: ((e: { component?: DevExpress.ui.dxDataGrid, element?: DevExpress.core.dxElement, model?: any, data?: any, key?: any, value?: any, displayValue?: string, text?: string, columnIndex?: number, column?: DevExpress.ui.dxDataGridColumn, rowIndex?: number, rowType?: string, row?: DevExpress.ui.dxDataGridRowObject, isSelected?: boolean, isExpanded?: boolean, cellElement?: DevExpress.core.dxElement }) => any);
    /** A function that is executed before a context menu is rendered. */
    onContextMenuPreparing?: ((e: { component?: DevExpress.ui.dxDataGrid, element?: DevExpress.core.dxElement, model?: any, items?: Array<any>, target?: string, targetElement?: DevExpress.core.dxElement, columnIndex?: number, column?: DevExpress.ui.dxDataGridColumn, rowIndex?: number, row?: DevExpress.ui.dxDataGridRowObject }) => any);
    /** A function that is executed before a cell or row switches to the editing state. */
    onEditingStart?: ((e: { component?: DevExpress.ui.dxDataGrid, element?: DevExpress.core.dxElement, model?: any, data?: any, key?: any, cancel?: boolean, column?: any }) => any);
    /** A function that is executed after an editor is created. */
    onEditorPrepared?: ((options: { component?: DevExpress.ui.dxDataGrid, element?: DevExpress.core.dxElement, model?: any, parentType?: string, value?: any, setValue?: any, updateValueTimeout?: number, width?: number, disabled?: boolean, rtlEnabled?: boolean, editorElement?: DevExpress.core.dxElement, readOnly?: boolean, dataField?: string, row?: DevExpress.ui.dxDataGridRowObject }) => any);
    /** A function that is executed before an editor is created. */
    onEditorPreparing?: ((e: { component?: DevExpress.ui.dxDataGrid, element?: DevExpress.core.dxElement, model?: any, parentType?: string, value?: any, setValue?: any, updateValueTimeout?: number, width?: number, disabled?: boolean, rtlEnabled?: boolean, cancel?: boolean, editorElement?: DevExpress.core.dxElement, readOnly?: boolean, editorName?: string, editorOptions?: any, dataField?: string, row?: DevExpress.ui.dxDataGridRowObject }) => any);
    /** A function that is executed after data is exported. */
    onExported?: ((e: { component?: DevExpress.ui.dxDataGrid, element?: DevExpress.core.dxElement, model?: any }) => any);
    /** A function that is executed before data is exported. */
    onExporting?: ((e: { component?: DevExpress.ui.dxDataGrid, element?: DevExpress.core.dxElement, model?: any, fileName?: string, cancel?: boolean }) => any);
    /** A function that is executed before a file with exported data is saved to the user's local storage. */
    onFileSaving?: ((e: { component?: DevExpress.ui.dxDataGrid, element?: DevExpress.core.dxElement, model?: any, fileName?: string, format?: string, data?: Blob, cancel?: boolean }) => any);
    /** A function that is executed when a user clicks a row. */
    onRowClick?: ((e: { component?: DevExpress.ui.dxDataGrid, element?: DevExpress.core.dxElement, model?: any, jQueryEvent?: JQueryEventObject, event?: DevExpress.event, data?: any, key?: any, values?: Array<any>, columns?: Array<any>, rowIndex?: number, rowType?: string, isSelected?: boolean, isExpanded?: boolean, groupIndex?: number, rowElement?: DevExpress.core.dxElement, handled?: boolean }) => any) | string;
    /** A function that is executed after the widget creates a row. */
    onRowPrepared?: ((e: { component?: DevExpress.ui.dxDataGrid, element?: DevExpress.core.dxElement, model?: any, data?: any, key?: any, values?: Array<any>, columns?: Array<DevExpress.ui.dxDataGridColumn>, rowIndex?: number, rowType?: string, groupIndex?: number, isSelected?: boolean, isExpanded?: boolean, rowElement?: DevExpress.core.dxElement }) => any);
    /** Notifies the DataGrid of the server's data processing operations. */
    remoteOperations?: boolean | { sorting?: boolean, filtering?: boolean, paging?: boolean, grouping?: boolean, groupPaging?: boolean, summary?: boolean } | 'auto';
    /** Specifies a custom template for rows. */
    rowTemplate?: DevExpress.ui.template | ((rowElement: DevExpress.core.dxElement, rowInfo: any) => any);
    /** Configures scrolling. */
    scrolling?: DevExpress.ui.dxDataGridScrolling;
    /** Configures runtime selection. */
    selection?: DevExpress.ui.dxDataGridSelection;
    /** Specifies filters for the rows that must be selected initially. Applies only if selection.deferred is true. */
    selectionFilter?: string | Array<any> | Function;
    /** Allows you to sort groups according to the values of group summary items. */
    sortByGroupSummaryInfo?: Array<{ summaryItem?: string | number, groupColumn?: string, sortOrder?: 'asc' | 'desc' }>;
    /** Specifies the options of the grid summary. */
    summary?: { groupItems?: Array<{ name?: string, column?: string, summaryType?: 'avg' | 'count' | 'custom' | 'max' | 'min' | 'sum', valueFormat?: DevExpress.ui.format, precision?: number, displayFormat?: string, customizeText?: ((itemInfo: { value?: string | number | Date, valueText?: string }) => string), showInGroupFooter?: boolean, alignByColumn?: boolean, showInColumn?: string, skipEmptyValues?: boolean }>, totalItems?: Array<{ name?: string, column?: string, showInColumn?: string, summaryType?: 'avg' | 'count' | 'custom' | 'max' | 'min' | 'sum', valueFormat?: DevExpress.ui.format, precision?: number, displayFormat?: string, customizeText?: ((itemInfo: { value?: string | number | Date, valueText?: string }) => string), alignment?: 'center' | 'left' | 'right', cssClass?: string, skipEmptyValues?: boolean }>, calculateCustomSummary?: ((options: { component?: DevExpress.ui.dxDataGrid, name?: string, summaryProcess?: string, value?: any, totalValue?: any }) => any), skipEmptyValues?: boolean, texts?: { sum?: string, sumOtherColumn?: string, min?: string, minOtherColumn?: string, max?: string, maxOtherColumn?: string, avg?: string, avgOtherColumn?: string, count?: string } };
}
