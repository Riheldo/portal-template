import { OnInit, OnChanges, Input, SimpleChanges, AfterViewChecked, EventEmitter } from '@angular/core';
import { VstDxItemSetting } from '../vst-form/interfaces/vst-dx-item-setter.interface';
import DevExpress from 'devextreme/bundles/dx.all';
import { CustomTemplateFormat } from '../vst-form/interfaces/vst-customtemplate';
import { VsBothSchema } from './vst-both-schema.interface';
import { transformField } from '../utils/transform';

export interface Doc {
    [key: string]: any;
}

/**
 * 
 * @param node Raiz onde será iniciada busca
 * @param searchQuery Chave onde eh mapeado os valores
 * @param searchKey Valor mapeado pela query
 */
function pathTo (node: any, searchQuery: string, searchKey: string): string {
    let result = dxFormPathTo(node, searchQuery, searchKey);
    return !result ? result : result.substr(1, result.length - 2)
}
function dxFormPathTo (node: object, searchQuery: string, searchKey: string): string {
    if (node instanceof Array) {
        if (!node.length) return "";
        for (let i = 0; i < node.length; i++) {
            const result = dxFormPathTo(node[i], searchQuery, searchKey);
            if (result) return "[" + i + "]" + result;
        }
    } else if (typeof node === "object" && (node as any).__proto__ === Object.prototype) {
        if (node[searchQuery] == searchKey) return ".";
        for (const auxField of Object.keys(node)) {
            if (node[auxField] === null || node[auxField] === undefined || !(typeof node[auxField] === 'object')) continue;
            const result = dxFormPathTo(node[auxField], searchQuery, searchKey);
            if (result) return "." + auxField + result;
        }
    }
    return "";
}

export abstract class VstCommomSchemaInitializeComponent<S, I> implements OnInit, OnChanges, AfterViewChecked {
    
    private _schema: S & VsBothSchema;
    @Input('schema')
    set schema(schema: S & VsBothSchema) {
        this._schema = schema;
    }
    get schema(): S & VsBothSchema {
        return this._schema;
    }

    format: CustomTemplateFormat;
    formatChange: EventEmitter<CustomTemplateFormat> = new EventEmitter();

    public dxInstance: any;
    public dxInstanceChanges: EventEmitter<void> = new EventEmitter();
    protected instanceInitialized: boolean = false;

    abstract customInitialize(): void;
    abstract handleFormat(): void;

    
    ngOnInit() {
        this.initializeInstance();
    }

    ngOnChanges(changes: SimpleChanges) {
        // if (changes && changes.schema && JSON.stringify(changes.schema.previousValue) !== JSON.stringify(changes.schema.currentValue)) {
        if (changes && changes.schema) {
            this.instanceInitialized = false;
            this.initializeInstance();
        }
    }

    ngAfterViewChecked() {
        this.initializeInstance();
    }

    beforeInitialize() {
        this.dxInstanceChanges.emit();
    }

    initializeInstance() {
        if (this.dxInstance && !this.instanceInitialized) {
            this.instanceInitialized = true;

            this.beforeInitialize();
            
            const schema = this.schema;
            for (const objKey in schema as Doc) {
                if (this.dxInstance[objKey] instanceof EventEmitter) {
                    (this.dxInstance[objKey] as EventEmitter<any>).subscribe(schema[objKey]);
                } else if (typeof this.dxInstance.option === "function") {
                    this.dxInstance.option(objKey, schema[objKey])
                } else this.dxInstance[objKey] = schema[objKey];
            }
            
            this.customInitialize();
        }
    }

    onInitialized(e: {component: DevExpress.ui.dxForm | DevExpress.ui.dxDataGrid}) {
        const instanceToSet: VstDxItemSetting = e.component.instance() as any;
        const instance = e.component.instance();

        instanceToSet.setItemField = (dataField: string, key: string, value: any, objUpType = "full") => {
            const itemPath: string = this.getItemFieldPath(dataField);
            if (typeof value === "object" && objUpType === "individual") {
                for (const subKey in value as object) {
                    instance.option(itemPath + "." + key + "." + subKey, value[subKey]);
                }
            } else {
                instance.option(itemPath + "." + key, value);
            };
        }

        instanceToSet.setItemName = (name: string, key: string, value: any, objUpType = "full") => {
            const itemPath: string = this.getItemNamePath(name);
            if (typeof value === "object" && objUpType === "individual") {
                for (const subKey in value as object) {
                    instance.option(itemPath + "." + key + "." + subKey, value[subKey]);
                }
            } else {
                instance.option(itemPath + "." + key, value);
            };
        };
        
        instanceToSet.setItem = (fieldKey: string, fieldValue: string, key: string, value: string, objUpType: string = "full") => {
            let itemPath: string = this.getPathTo(fieldKey, fieldValue);
            if (!itemPath) return;
            itemPath += "." + key;
            if (typeof value === "object" && objUpType === "individual") {
                for (const subKey in value as object) {
                    instance.option(itemPath + "." + subKey, value[subKey]);
                }
            } else {
                instance.option(itemPath, value);
            };
        }

        instanceToSet.setChanges = (changesFunction: () => void) => {
            instance.beginUpdate();
            try {
                changesFunction();
                instance.endUpdate();
            } catch (error) {
                instance.endUpdate();
                throw error;
            }
        };

        instanceToSet.getItem = (fieldKey: string, fieldValue: string) => {
            let itemPath: string = this.getPathTo(fieldKey, fieldValue);
            return instance.option(itemPath); 
        }
        instanceToSet.getItemName = (name: string) => {
            const itemPath: string = this.getItemNamePath(name);
            return instance.option(itemPath);
        }
        instanceToSet.getItemField = (dataField: string) => {
            const itemPath: string = this.getItemFieldPath(dataField);
            return instance.option(itemPath);
        }

        if (this.schema && (this.schema as any).onInitialized && typeof (this.schema as any).onInitialized === "function") {
            (this.schema as any).onInitialized(e);
        }
    }

    getItemFieldPath(dataField: string): string {
        return pathTo(this.dxInstance.instance.option(), 'dataField', dataField);
    }
    getItemNamePath(name: string): string {
        return pathTo(this.dxInstance.instance.option(), 'name', name);
    }
    
    getPathTo(field: string, key: string): string {
        return pathTo(this.dxInstance.instance.option(), field, key);
    }


    // TRATAMENTO DE TRANSFORMACAO DE CAMPO
    dataTrasformation(data) {
        (this.schema)
        const allFields = Object.keys(data);
        if (this.schema.transform && this.schema.transform.pipes && this.schema.transform.pipes.length) {
            // preparar para transformar campos
            this.schema.transform.pipes.forEach((fieldTransform) => {
                if (!fieldTransform.fields.length) return;

                let fieldsToTransform: Array<string> = [];
                if (fieldTransform.fields === "*") {
                    // executar todos os campos PRESENTES EM DATA, verificar excecoes
                    fieldsToTransform = allFields.filter((field) => {
                        return !(fieldTransform.exceptions && fieldTransform.exceptions.indexOf(field) !== -1)
                    });
                } else {
                    fieldsToTransform = fieldTransform.fields;
                }

                for (const field of fieldsToTransform) {
                    try {
                        transformField(field, data, fieldTransform.use);
                    } catch (error) {
                        if (this.schema.transform.ignoreError === false) {
                            throw error;
                        } else {
                            console.warn("Erro na transformacao do campo '"+field+"' Transformação ignorada, valor default usado! \nErro: ", error);
                        }
                    }
                }
            })
        }
    }

    insertCssInNode(itemNode: {cssClass?: string, dataField?: string}) {
        if (this.schema.transform && this.schema.transform.pipes && this.schema.transform.pipes.length) {
                
            // iniciar aplicacao de classes vazia
            let applyCssClasses: Array<string> = [];

            this.schema.transform.pipes.forEach((pipeTransform) => {
                // caso nao tenha fields, saia do pipe
                if (!pipeTransform.fields.length) return;

                // se houver classe para pipe, aplicar
                if (pipeTransform.cssClass || typeof pipeTransform.use === 'string') {
                    if (
                        (pipeTransform.fields === "*" && (!pipeTransform.exceptions || !pipeTransform.exceptions.filter((nm) => nm == itemNode.dataField).length)) || 
                        ((pipeTransform.fields instanceof Array) && pipeTransform.fields.filter((name) => name==itemNode.dataField).length)
                    ) {
                        const cssToFill: string = (pipeTransform.cssClass) ? pipeTransform.cssClass : pipeTransform.use as string;
                        applyCssClasses.push(cssToFill);
                    }
                }
            });

            // realizar aplicacoes de classes se houver
            if (applyCssClasses.length) {
                applyCssClasses.unshift("vs-simplecolumn");
                if (!itemNode.cssClass) itemNode.cssClass = "";
                itemNode.cssClass += " " + applyCssClasses.join(" ");
            }
        }
    }

}
