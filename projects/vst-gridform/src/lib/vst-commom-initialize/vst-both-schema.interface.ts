import { VsTransform } from '../vst-form/interfaces/vst-transform.interface';

export interface VsBothSchema {
    transform?: VsTransform;
}