import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VSTdxFormComponent } from './vst-form.component';
import { DxFormModule } from 'devextreme-angular/ui/form';
import { DxValidatorModule } from 'devextreme-angular/ui/validator';
import { VstDxItemContainer } from './templates/vst-dxitemcontainer.component';
import { DxPopupModule } from 'devextreme-angular/ui/popup';

@NgModule({
    imports: [
        CommonModule,
        DxFormModule,
        DxValidatorModule,
        DxPopupModule,
    ],
    declarations: [
        VSTdxFormComponent,
        VstDxItemContainer
    ],
    entryComponents: [
        VstDxItemContainer
    ],
    exports: [
        VSTdxFormComponent
    ]
})
export class VSTFormModule { }
