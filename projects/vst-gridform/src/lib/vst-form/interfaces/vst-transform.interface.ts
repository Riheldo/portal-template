export interface VsTransform {
    ignoreError?: boolean;
    pipes?: Array<
        {fields: "*", exceptions?: Array<string>, use: "capitalize" | "titleize" | "uppercase" | "lowercase" | ((v: any) => any), cssClass?: string} | 
        {fields: Array<string>, use: "capitalize" | "titleize" | "uppercase" | "lowercase" | ((v: any) => any), cssClass?: string}
    >;
}