import DevExpress from "devextreme/bundles/dx.all";
import { VSTFormItems } from "./vst-customtemplate";
import { VstDxForm } from "./vst-dxform.interface";
import { VsTransform } from './vst-transform.interface';
import { SbrDyn } from '../engine/vst-state-based-result';

export enum VsPartialLoadMode {
    ASK = "ask",
    ALWAYS = "always",
}

export interface VsDynOptMap {
    [key: string]: SbrDyn;
}

export interface VSTFormSchema extends DevExpress.ui.WidgetOptions<VstDxForm> {
    
    /////// OPCOES PROPRIAS CUSTOMIZADAS

    // transformacoes de texto antes de salvar
    transform?: VsTransform;
    savePartial?: {
        active: boolean;
        saveas?: string;
        loadMode?: VsPartialLoadMode;
        onOperation?: "insert";
        askMessage?: string;
        delay?: number;
    };
    dynamicOptions?: {
        fieldMap?: VsDynOptMap,
        nameMap?: VsDynOptMap,
    }

    ///// DEVEXTREME -----------------------------------

    /** Specifies whether or not all root item labels are aligned. */
    alignItemLabels?: boolean;
    /** Specifies whether or not item labels in all groups are aligned. */
    alignItemLabelsInAllGroups?: boolean;
    /** The count of columns in the form layout. */
    colCount?: number | 'auto';
    /** Specifies dependency between the screen factor and the count of columns in the form layout. */
    colCountByScreen?: {
        lg?: number;
        md?: number;
        sm?: number;
        xs?: number;
    };
    /** Specifies a function that customizes a form item after it has been created. */
    // customizeItem?: ((item: DevExpress.ui.dxFormSimpleItem | DevExpress.ui.dxFormGroupItem | DevExpress.ui.dxFormTabbedItem | DevExpress.ui.dxFormEmptyItem | DevExpress.ui.dxFormButtonItem) => any);
    customizeItem?: ((item: VSTFormItems) => any);
    /** Provides the Form's data. Gets updated every time form fields change. */
    formData?: any;
    /** Holds an array of form items. */
    // items?: Array<DevExpress.ui.dxFormSimpleItem | DevExpress.ui.dxFormGroupItem | DevExpress.ui.dxFormTabbedItem | DevExpress.ui.dxFormEmptyItem | DevExpress.ui.dxFormButtonItem>;
    items?: VSTFormItems;
    /** Specifies the location of a label against the editor. */
    labelLocation?: 'left' | 'right' | 'top';
    /** The minimum column width used for calculating column count in the form layout. */
    minColWidth?: number;
    /** A handler for the editorEnterKey event. */
    onEditorEnterKey?: ((e: { component?: DevExpress.ui.dxForm, element?: DevExpress.core.dxElement, model?: any, dataField?: string }) => any);
    /** A handler for the fieldDataChanged event. */
    onFieldDataChanged?: ((e: { component?: DevExpress.ui.dxForm, element?: DevExpress.core.dxElement, model?: any, dataField?: string, value?: any }) => any);
    /** The text displayed for optional fields. */
    optionalMark?: string;
    /** Specifies whether all editors on the form are read-only. Applies only to non-templated items. */
    readOnly?: boolean;
    /** The text displayed for required fields. */
    requiredMark?: string;
    /** Specifies the message that is shown for end-users if a required field value is not specified. */
    requiredMessage?: string;
    /** Specifies the function returning the screen factor depending on the screen width. */
    screenByWidth?: Function;
    /** A Boolean value specifying whether to enable or disable form scrolling. */
    scrollingEnabled?: boolean;
    /** Specifies whether or not a colon is displayed at the end of form labels. */
    showColonAfterLabel?: boolean;
    /** Specifies whether or not the optional mark is displayed for optional fields. */
    showOptionalMark?: boolean;
    /** Specifies whether or not the required mark is displayed for required fields. */
    showRequiredMark?: boolean;
    /** Specifies whether or not the total validation summary is displayed on the form. */
    showValidationSummary?: boolean;
    /** Gives a name to the internal validation group. */
    validationGroup?: string;
}