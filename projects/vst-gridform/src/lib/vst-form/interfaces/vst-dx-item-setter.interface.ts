export interface VstDxItemSetting {
    setItemField: (dataField: string, key: string, value: any, objUpType?: "full" | "individual") => void;
    setItemName: (name: string, key: string, value: any, objUpType?: "full" | "individual") => void;
    setItem: (fieldKey: string, fieldValue: string, value: string, objUpType?: "full" | "individual") => void;
    setChanges: (changesFunction: () => void) => void;
    getItemField: (dataField: string) => any;
    getItemName: (name: string) => any;
    getItem: (fieldKey: string, fieldValue: string) => any;
}