import { Input, Output, EventEmitter, Type } from "@angular/core";
import DevExpress from "devextreme/bundles/dx.all";

export type CustomTemplateFormat = 'readonly' | 'edit';
export abstract class VSTCustomTemplateComponent <T> {
    @Input() value: T;
    @Output() valueChange: EventEmitter<T> = new EventEmitter();
    public validationGroupName: string;
    public validationGroupNameChange: EventEmitter<string> = new EventEmitter();
    setValidationGroupName(groupName: string) {
        this.validationGroupName = groupName;
        this.validationGroupNameChange.emit(groupName);
    }
    public format: CustomTemplateFormat;
    public formatChange: EventEmitter<CustomTemplateFormat> = new EventEmitter();
}
// abstracoes complexas para template, necessita mais analises!
// export abstract class VSTComplexCustomTemplateComponent<T> extends VSTCustomTemplateComponent<T> {
//     __complex='true';
//     @Input() schema: VSTFormItems;
//}

export interface VSTTemplateComponentRegister {
    component: Type<VSTCustomTemplateComponent<any>>;
    inputs?: Array<string>;
}
export interface VSTTemplateRegister {
    [key: string]: VSTTemplateComponentRegister;
}

//
//// DEFINICAO DE TIPOS

// correct itemType from devextreme empty, group, simple, tabbed, button
export interface VstdxFormItem extends DevExpress.ui.dxFormSimpleItem {
    itemType?: "simple";
    editorType?: 'dxAutocomplete' | 'dxCalendar' | 'dxCheckBox' | 'dxColorBox' | 'dxDateBox' | 'dxDropDownBox' | 'dxLookup' | 'dxNumberBox' | 'dxRadioGroup' | 'dxRangeSlider' | 'dxSelectBox' | 'dxSlider' | 'dxSwitch' | 'dxTagBox' | 'dxTextArea' | 'dxTextBox';
}
export interface VstdxFormGroup extends DevExpress.ui.dxFormGroupItem {
    itemType: "group";
}
export interface VstcdxFormArray {
    /** Specifies whether or not all group item labels are aligned. */
    alignItemLabels?: boolean;
    /** Specifies the group caption. */
    caption?: string;
    /** The count of columns in the group layout. */
    colCount?: number;
    /** Specifies dependency between the screen factor and the count of columns in the group layout. */
    colCountByScreen?: any;
    /** Specifies the number of columns spanned by the item. */
    colSpan?: number;
    /** Specifies a CSS class to be applied to the form item. */
    cssClass?: string;
    /** Holds an array of form items displayed within the group. */
    items?: VSTFormItems;
    /** Specifies the item's type. Set it to "group" to create a group item. */
    itemType?: "array";
    /** Specifies a name that identifies the form item. */
    name?: string;
    /** A template to be used for rendering a group item. */
    // template?: template | ((data: { component?: dxForm, formData?: any }, itemElement: DevExpress.core.dxElement) => string | Element | JQuery);
    /** Specifies whether or not the current form item is visible. */
    visible?: boolean;
    /** Specifies the sequence number of the item in a form, group or tab. */
    visibleIndex?: number;
}

export interface VstdxFormTabbed extends DevExpress.ui.dxFormTabbedItem {
    itemType: "tabbed";
}
export interface VstdxFormEmpty extends DevExpress.ui.dxFormEmptyItem {
    itemType: "empty";
}
export interface VstdxFormButton extends DevExpress.ui.dxFormButtonItem {
    itemType: "button";
}

export type VSTFormItem = VstdxFormItem | VstdxFormGroup | VstcdxFormArray | VstdxFormTabbed | VstdxFormEmpty | VstdxFormButton;
export type VSTFormItems = Array<VSTFormItem>;