import { SbrDyn } from './vst-state-based-result';
import { EventEmitter } from '@angular/core';
import { VstDxForm } from '../interfaces/vst-dxform.interface';


export class VsStateEventController {
    
    fieldsTriggers: {[key: string]: EventEmitter<VstDxForm>} = {};
    nameTriggers: {[key: string]: EventEmitter<VstDxForm>} = {};

    dynamics:Array<SbrDyn> = [];

    private coldStartRuned: boolean = false;

    private _lock: boolean;
    get locked(): boolean {
        return this._lock;
    }

    lock() {
        this._lock = true;
    }

    addOnFieldTriggers() {}
    
    add(dyn: SbrDyn, isField: boolean, path: string) {
        const obs = dyn.getObservers();
        const searchKeyPathArray = path.split('.');
        const searchKey: string = searchKeyPathArray.shift();
        const searchPath: string = searchKeyPathArray.join('.');
        for (const auxob of obs) {
            if (
                (isField && auxob.searchMode === "field" && auxob.searchKey === searchKey) ||
                (!isField && auxob.searchMode === "name" && auxob.searchKey === searchKey)
            ) throw new Error("Um dinamico nao pode observar ele mesmo! Suposicao errada");
        }

        dyn.getObservers().forEach((item) => {
            if (item.searchMode === "field") {
                if (!this.fieldsTriggers[item.searchKey]) this.fieldsTriggers[item.searchKey] = new EventEmitter();
                this.fieldsTriggers[item.searchKey].subscribe((widget: VstDxForm) => {
                    const val = dyn.resolveFilter(widget);

                    if (isField) {
                        widget.setItemField(searchKey, searchPath, val);
                    } else {
                        widget.setItemName(searchKey, searchPath, val);
                    }
                })
            }
        });
        dyn.getSignals().forEach((obItem) => {
            obItem.subscribe((widget: VstDxForm) => {
                const val = dyn.resolveFilter(widget);

                if (isField) {
                    widget.setItemField(searchKey, searchPath, val);
                } else {
                    widget.setItemName(searchKey, searchPath, val);
                }
            });
        })
    }

    notifyOptionChanged (e: {component: VstDxForm, fullName: string, name: string, value: any}) {
        if (e.name === "formData") return;
        
    }

    notifyFieldDataChanged (e: {component: VstDxForm, dataField: string, value: any}) {
        if (this.fieldsTriggers[e.dataField]) {
            this.fieldsTriggers[e.dataField].emit(e.component);
        }
    }

    coldStart(dxWidget: VstDxForm) {
        Object.keys(this.fieldsTriggers).forEach((fieldKey) => {
            this.fieldsTriggers[fieldKey].emit(dxWidget);
        })
        Object.keys(this.nameTriggers).forEach((nameKey) => {
            this.nameTriggers[nameKey].emit(dxWidget);
        })
    }
    runColdStart(dxWidget: VstDxForm) {
        if (this.coldStartRuned) return;
        this.coldStart(dxWidget);
    }
}