import { VstDxForm } from '../interfaces/vst-dxform.interface';
import DevExpress from 'devextreme/bundles/dx.all';
import { Observable } from 'rxjs';

export enum VsConditional {
    OR = "OR",
    AND = "AND"
}
// export interface VsDynamicFilter {
//     operator?: "AND" | "OR";
//     selectors: Array<VsSelector | VsDynamicFilter>;
// }

export interface EditorObserver {
    watchKey: string;
    searchMode: "field" | "name";
    searchKey: string;
}
export type EditorObservers = Array<EditorObserver>;

function getObs(selectors: Array<SbrSel | SbrCon>): EditorObservers {
    const lObs: EditorObservers = [];
    const lTmp: Array<EditorObservers> = []
    for (const sel of selectors) {
        if (sel instanceof SbrCon) {
            lTmp.push(getObs(sel.selectors));
        } else {
            if (sel.searchMode === "name") lObs.push({searchKey: sel.searchKey, searchMode: "name", watchKey: (sel.isValue) ? "value" : sel.watchKey});
            if (sel.searchMode === "field") lObs.push({searchKey: sel.searchKey, searchMode: "field", watchKey: (sel.isValue) ? "value" : sel.watchKey});
        }
    }
    if (lTmp.length) lTmp.forEach((tmpObs) => {
        tmpObs.forEach((tmpo) => {
            for (const lob of lObs) {
                if (tmpo.searchKey == lob.searchKey && tmpo.searchMode == lob.searchMode && tmpo.watchKey == lob.watchKey) {
                    return;
                }
            }
            lObs.push(tmpo);
        })
    })
    return lObs;
}

function getSignals(selectors: Array<SbrSel | SbrCon>): Array<Observable<VstDxForm>> {
    const lObs: Array<Observable<VstDxForm>> = [];
    const lTmp: Array<Array<Observable<VstDxForm>>> = []
    for (const sel of selectors) {
        if (sel instanceof SbrCon) {
            lTmp.push(getSignals(sel.selectors));
        } else {
            if (sel.searchMode !== "signal") continue;
            lObs.push(sel.sign);
        }
    }
    if (lTmp.length) lTmp.forEach((tmpObs) => {
        tmpObs.forEach((tmpo) => {
            for (const lob of lObs) {
                if (tmpo === lob) {
                    return;
                }
            }
            lObs.push(tmpo);
        })
    });
    return lObs;
}

function analyzeValue (actualData: any, expectedData: any, validation?: boolean): boolean {
    if (typeof expectedData === "function") {
        return expectedData({actualData: actualData, validation: validation});
    }
    if (typeof expectedData === "string" && expectedData[0] === "$") {
        if (expectedData === "$defined") return !!actualData;
        if (expectedData === "$undefined") return !actualData;
        if (expectedData === "$valid") {
            if (validation === undefined) return false;
            return validation;
        }
        if (expectedData === "$invalid") {
            if (validation === undefined) return false
            return !validation;
        }
    } else {
        return actualData == expectedData;
    }
    return false;
}

function resolveCondDynFilter(dxWidget: VstDxForm, cond: SbrCon): boolean {
    for (const sel of cond.selectors) {
        let result: boolean;
        if (sel instanceof SbrCon) {
            result = resolveCondDynFilter(dxWidget, sel);
        } else {

            /// validacao de campo
            let fieldValid: boolean = sel.getFieldValid(dxWidget);

            /// captura de valor atual
            let selectionVal = sel.getActualValue(dxWidget);
            
            result = analyzeValue(selectionVal, sel.valExpected, fieldValid);
        }
        if (cond.conditional === VsConditional.OR && result) return true;
        if (cond.conditional === VsConditional.AND && !result) return false; 
    }
    if (cond.conditional === VsConditional.OR) return false;
    return true;
}

export class SbrCon {
    selectors: Array<SbrSel | SbrCon> = [];
    conditional: VsConditional = VsConditional.AND;

    static AND(...a: Array<SbrSel | SbrCon>): SbrCon {
        const auxDyn = new SbrCon();
        auxDyn.conditional = VsConditional.AND
        auxDyn.selectors = a;
        return auxDyn;
    }
    static OR(...a:  Array<SbrSel | SbrCon>): SbrCon {
        const auxDyn = new SbrCon();
        auxDyn.conditional = VsConditional.OR
        auxDyn.selectors = a;
        return auxDyn;
    }
}


// VsDynamic
export class SbrDyn {
    cond: SbrCon;
    val: any;
    valelse: any;

    getObservers(): EditorObservers {
        return getObs(this.cond.selectors)
    }

    getSignals(): Array<Observable<VstDxForm>> {
        return getSignals(this.cond.selectors);
    }

    resolveFilter(dxWidget: VstDxForm): any {
        const result = resolveCondDynFilter(dxWidget, this.cond);
        if (result) return this.val
        return this.valelse;
    }

    static TrueIf(selector: SbrSel | SbrCon): SbrDyn {
        return SbrDyn.ValueIf(true, false, selector);
    }
    static FalseIf(selector: SbrSel | SbrCon): SbrDyn {
        return SbrDyn.ValueIf(false, true, selector);
    }
    static ValueIf(val: any, valelse: any, selector: SbrSel | SbrCon): SbrDyn {
        const dyn = new SbrDyn();
        dyn.val = val;
        dyn.valelse = valelse;

        let con: SbrCon;
        if (selector instanceof SbrSel) {
            con = SbrCon.AND(selector);
        } else con = selector;
        dyn.cond = con;
        
        return dyn;
    }
}

// VsSelector
export class SbrSel {
    valExpected: any;
    searchMode: "field" | "name" | "signal";
    searchKey: string;
    watchKey: string;

    private selectionValue: () => any;
    sign: Observable<VstDxForm>;

    get isField(): boolean {
        return this.searchMode === "field";
    }
    get isValue(): boolean {
        return this.watchKey == undefined || this.watchKey === "value";
    }

    getActualValue(dxWidget: VstDxForm): any {
        if (this.searchMode === "signal") {
            if (typeof this.selectionValue == "function") return this.selectionValue();
        } 
        if (this.isValue) {
            if (!this.isField) throw new Error("Para verificacao de valor o selectore precisa ser field");

            return dxWidget.option('formData.'+this.searchKey);

        } else {
            let dataObject;
            if (this.isField) {
                dataObject = dxWidget.getItemField(this.searchKey);
            } else {
                dataObject = dxWidget.getItemName(this.searchKey);
            }

            return dataObject[this.watchKey];
        }
    }

    getFieldValid(dxWidget: VstDxForm): boolean {
        if (this.isField) {
            const editorField = dxWidget.getEditor(this.searchKey);
            if (editorField) {
                return editorField.option('isValid');
            }
        }
        return undefined;
    }

    static byField(field: string, valExpected: any, watchKey?: string): SbrSel {
        const auxSelector = new SbrSel();
        auxSelector.searchMode = "field";
        auxSelector.searchKey = field;
        auxSelector.valExpected = valExpected;
        auxSelector.watchKey = watchKey;
        return auxSelector;
    }
    static byName(name: string, valExpected: any, watchKey?: string): SbrSel {
        const auxSelector = new SbrSel();
        auxSelector.searchMode = "name";
        auxSelector.searchKey = name;
        auxSelector.valExpected = valExpected;
        auxSelector.watchKey = watchKey;
        return auxSelector;
    }

    bySignal(sign: Observable<VstDxForm>, valSelection: (() => any), valExpected: any): SbrSel;
    bySignal(sign: Observable<VstDxForm>, valExpected: (() => boolean)): SbrSel;
    bySignal(sign: Observable<VstDxForm>, val1: (() => boolean) | (() => any), val2?: any): SbrSel {
        const auxSelector = new SbrSel();
        let valSelection, valExpected;

        if (!val2) valExpected = val1;
        else {
            valSelection = val1;
            valExpected = val2;
        }

        auxSelector.searchMode = "signal";
        auxSelector.valExpected = valExpected;
        auxSelector.selectionValue = valSelection;
        auxSelector.sign = sign;
        return auxSelector;
    }
}