import { Injector, ComponentFactoryResolver, ApplicationRef, EmbeddedViewRef, EventEmitter, ComponentRef, ComponentFactory, SimpleChanges, SimpleChange, OnChanges, ɵConsole } from "@angular/core";
import DevExpress from "devextreme/bundles/dx.all";
import { VSTTemplateComponentRegister } from "../interfaces/vst-customtemplate";
import { VSTTemplatesRegister } from "../templates/vst-dxCTemplates";
import { VSTCustomTemplateComponent } from '../interfaces/vst-customtemplate';
import { VstDxItemContainer } from '../templates/vst-dxitemcontainer.component';
import { Subscription } from 'rxjs';

// CRIA INTERFACE DE CONTRATO PARA COMPONENTES PODEREM AMARRAR TEMPLATES
export interface VstdxDataComponent<T> {
    data: T;
    dataChange: EventEmitter<T>;
    onValueChanged: EventEmitter<any>;
}



/**
 * 
 * SEXTA-FEIRA, DIA 04 DE OUTUBRO DE 2019
 * 
 * Falhei em tentar deixar o codigo mais performatico, porém corrigi o bug da nao destruicao do componente
 * Não foi facil!
 * 
 * O meio campo ajuda a ter uma separacao, se precisar ter um componente onde eh contante, temos sem depender do devextreme.
 * 
 * Também não consegui colocar changes para os inputs sem ser o value
 * 
 * CUIDADO COM O CODIGO ABAIXO, ELE EH MALICIOSO!
 * 
 */



/// CLOSURE
// funcao responsavel para amarrar o componente customizado como se fosse um editor do devextreme
export function tieDxComponentTemplate<T>(injector: Injector, dataComponent: VstdxDataComponent<T>, cbComponent: (cp: VSTCustomTemplateComponent<any>, instance: VstDxItemContainer) => void) {
    // ainda necessita do componente que segura o dado e o injector
    // make function 
    const componentFactoryResolver: ComponentFactoryResolver = injector.get(ComponentFactoryResolver);
    const appRef: ApplicationRef = injector.get(ApplicationRef);
    let itemContainers: Array<ComponentRef<any>> = [];
    
    return (dxdata: { component?: DevExpress.ui.dxForm, dataField?: string, editorOptions?: any, editorType?: string }, dxitemElement: DevExpress.core.dxElement) => {
        if (dxdata.editorType.substr(0, 3) == 'vst') {
            const templateRepr: VSTTemplateComponentRegister = VSTTemplatesRegister.get().retriveTemplate(dxdata.editorType); // circular dependence
            if (!templateRepr) {
                console.error("Template " + dxdata.editorType + " não se encontra nos registros de VSTTemplatesRegister.");
                return dxdata.editorType;
            }

            /////// INSERIR ITEM DE MEIO CAMPO
            // importante para tarefas de middleware como validacao que vem da grid
            const itemContainerRef = componentFactoryResolver.resolveComponentFactory(VstDxItemContainer).create(injector);
            /////// CRIAR COMPONENTE FINAL
            // generate component
            const componentRef = componentFactoryResolver.resolveComponentFactory(templateRepr.component).create(injector);
            /////// INSERIR COMPONENTE FINAL DENTRO DO INTERMEDIARIO
            itemContainerRef.instance.container.insert(componentRef.hostView);
            // (itemContainerRef.location.nativeElement as HTMLElement).id = elId;


            //// RESERVAR ENDERECOS PARA MUDANCAS DE INPUT
            const componentSimpleChange: SimpleChange = new SimpleChange(undefined, undefined, true);
            const inputChanges: {[key: string]: SimpleChange} = {};
            const subs: Subscription = new Subscription();

            // atribuir nome da validacao
            if (dataComponent['validationGroupName']) componentRef.instance.setValidationGroupName(dataComponent['validationGroupName']);

            subs.add(
                // subscribing data changes on parent
                dataComponent.dataChange.subscribe((v) => {
                    // ao ter alteração no valor do pai
                    // eh atualizado o valor 
                    componentRef.instance.value = v[dxdata.dataField];
                    itemContainerRef.instance.checkToRevaluate(); // revaluate validation

                    // prepate to call OnChanges
                    componentSimpleChange.previousValue = componentSimpleChange.currentValue;
                    componentSimpleChange.currentValue = v[dxdata.dataField];
                    // chamar OnChanges se existir
                    if (typeof (componentRef.instance as unknown as OnChanges).ngOnChanges === 'function') {
                        (componentRef.instance as unknown as OnChanges).ngOnChanges({value: componentSimpleChange});
                    }
                })
            );
            subs.add(
                // subscribing value change on child
                componentRef.instance.valueChange.subscribe((v) => {
                    // instancia avisa quanto tiver alteração no valor dela
                    // assim essa rotina atualiza o valor na datra do componente pai
                    dataComponent.data[dxdata.dataField] = v;
                    itemContainerRef.instance.checkToRevaluate(); // revaluate validation
                    dataComponent.onValueChanged.emit({component: dataComponent, dataField: dxdata.dataField, element: componentRef.location, value: v});
                }
            ));

            // apply data on instance
            componentRef.instance.value = dataComponent.data[dxdata.dataField];
            componentSimpleChange.currentValue = dataComponent.data[dxdata.dataField];
            if (typeof (componentRef.instance as unknown as OnChanges).ngOnChanges === 'function') {
                (componentRef.instance as unknown as OnChanges).ngOnChanges({value: componentSimpleChange});
            }

            // inserir inputs
            if (templateRepr.inputs && templateRepr.inputs.length > 0 && dxdata.editorOptions) {
                for (const inputKey of templateRepr.inputs) {
                    // INICIALIZAR SIMPLECHANGE PARA INPUTS
                    if ((dxdata.editorOptions as Object).hasOwnProperty(inputKey)) {
                        componentRef.instance[inputKey] = dxdata.editorOptions[inputKey];
                        if (!inputChanges[inputKey]) inputChanges[inputKey] = new SimpleChange(undefined, dxdata.editorOptions[inputKey], true);
                        else {
                            inputChanges[inputKey].previousValue = inputChanges[inputKey].currentValue;
                            inputChanges[inputKey].currentValue = dxdata.editorOptions[inputKey];
                        }

                        // chamar OnChanges se existir
                        if (typeof (componentRef.instance as unknown as OnChanges).ngOnChanges === 'function') {
                            const doc = {};
                            doc[inputKey] = inputChanges[inputKey];
                            (componentRef.instance as unknown as OnChanges).ngOnChanges(doc);
                        }
                    }
                }
            }
            

            ///// OBSERVER PARA DESTROIR COMPONENTE (muitas horas investidas aqui)
            // importante para angular poder chamar ondestroy
            // obs: Nao deu certo guardar o ultimo e sempre retornar o mesmo, crianda assim apenas um valor possivel. O html quebra!
            itemContainers.push(itemContainerRef);
            if (itemContainers.length > 1) {
                const oldCont = itemContainers.shift();
                oldCont.destroy();
            }
            dxdata.component.on('disposing', (e) => {
                if (itemContainers) {
                    itemContainers.forEach(cont => cont.destroy());
                    itemContainers = null;
                }
            });

            ///// SUBIR NODES PARA DOM TREE
            ///// assim o angular pode gerenciar o componente
            appRef.attachView(itemContainerRef.hostView);

            ////// CHAMAR CALLBACK
            ////// aguardar componentes estarem presentes na DOM TREE do angular para chamar o callback
            setTimeout(() => {
                cbComponent(componentRef.instance, itemContainerRef.instance);
            });

            //// Returning DOM Element
            const domElem = (itemContainerRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
            return domElem;
        }
        console.error("Template '" + dxdata.editorType + "' nao reconhecido.");
        return dxdata.editorType;
    };
}
