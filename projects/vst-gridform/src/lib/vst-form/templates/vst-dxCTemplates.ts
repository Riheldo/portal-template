import { VSTTemplateRegister, VSTTemplateComponentRegister } from "../interfaces/vst-customtemplate";

export class VSTTemplatesRegister {
    private static instance: VSTTemplatesRegister = undefined;
    private templates: VSTTemplateRegister = {};

    static get(): VSTTemplatesRegister {
        if (!VSTTemplatesRegister.instance) {
            VSTTemplatesRegister.instance = new VSTTemplatesRegister();
        }
        return VSTTemplatesRegister.instance;
    }
    
    addTemplate(name: string, component: any, inputs: Array<string> = []): void {
        if (this.templates[name]) {
            console.error('Duplicate! Template with name: ' + name + " already exist!");
            // throw new Error('Duplicate! Template with name: ' + name + " already exist!");
        }
        this.templates[name] = {component: component, inputs: inputs};
    }

    retriveTemplate(name: string): VSTTemplateComponentRegister {
        return this.templates[name];
    }
}
