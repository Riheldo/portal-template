import { ViewChild, Component, OnInit, ViewContainerRef } from '@angular/core';
import { DxValidatorComponent } from 'devextreme-angular/ui/validator';

@Component({
    selector: 'vst-dxitem-container',
    template: '<div class="vst-dxitemcontainer"><ng-container #dxitemcontainer></ng-container><dx-validator></dx-validator></div>',
    styles: [`.vst-dxitemcontainer {border-radius: 4px;}`]
})
export class VstDxItemContainer implements OnInit {
    @ViewChild(DxValidatorComponent, {static: true}) validator: DxValidatorComponent;
    @ViewChild('dxitemcontainer', {static: true, read: ViewContainerRef}) container: ViewContainerRef;
    
    validationGroup: string;
    adapterGetValue: Function;
    readyToRevalidate: boolean = false;
    
    ngOnInit() {
        this.validator.adapter = {
            applyValidationResults: (e) => {
                if (e.isValid) this.reset();
                else this.setBorder('#f0bab8');
            },
            reset: (e) => {
                this.reset();
            },
            focus: () => {
                const el = (this.container.element.nativeElement as HTMLElement).parentElement;

                this.setBorder('#d9534f');
                setTimeout(() => {
                    this.setBorder('#f0bab8');
                }, 3000);

                el.scrollIntoView();
            },
            getValue: this.adapterGetValue,
        }
        if (this.validationGroup) this.validator.validationGroup = this.validationGroup
        this.validator.onValidated.subscribe(() => {
            this.readyToRevalidate = true;
        });
    }

    reset() {
        this.setBorder();
    }

    revaluate() {
        if (!this.readyToRevalidate) return;
        this.validator.instance.validate();
    }

    checkToRevaluate() {
        setTimeout(() => {
            this.revaluate();
        });
    }

    finishKnot() {
        if (this.validator) {
            this.validator.validationGroup = this.validationGroup;
            if (this.validator.adapter) {
                this.validator.adapter.getValue = this.adapterGetValue;
            }
        }
    }

    setBorder(color?: string) {
        (this.container.element.nativeElement as HTMLElement).parentElement.style.setProperty('border', color ? '1px solid ' + color : '');
    }
}