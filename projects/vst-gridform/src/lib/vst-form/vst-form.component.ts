import { Component, Input, Output, EventEmitter, Injector, ViewChild, ElementRef, ViewContainerRef, OnInit } from '@angular/core';
import { DxFormComponent } from 'devextreme-angular/ui/form';
import { VSTFormItems, VSTFormItem, VstdxFormItem, VstdxFormGroup, VstcdxFormArray, VstdxFormTabbed, VSTCustomTemplateComponent } from './interfaces/vst-customtemplate';
import { tieDxComponentTemplate, VstdxDataComponent } from './engine/vst-tiedxcomponent';
import { VSTFormSchema, VsPartialLoadMode } from './interfaces/vst-form-schema.interface';
import { VstCommomSchemaInitializeComponent } from '../vst-commom-initialize/vst-commom-initialize.component';
import { VstGlobalFormConfig } from '../utils/default-form.function';
import { smartAssign } from '../utils/smart-assign.function';
import DevExpress from 'devextreme/bundles/dx.all';
import { DxPopupComponent } from 'devextreme-angular/ui/popup';
import { VsStateEventController } from './engine/vst-event-controller';
import { SbrDyn } from './engine/vst-state-based-result';
import validationEngine from 'devextreme/ui/validation_engine';

@Component({
    selector: 'vst-form',
    templateUrl: './vst-form.component.html'
})
export class VSTdxFormComponent<T = any> extends VstCommomSchemaInitializeComponent<VSTFormSchema, DxFormComponent> implements VstdxDataComponent<T> {

    @ViewChild(DxFormComponent, {static: false}) dxInstance: DxFormComponent;
    @ViewChild('popupDataRecoverConfirmation', {static: false}) popupDataRecoverConfirmation: DxPopupComponent;

    @Input() formOperation: "create" | "read" | "update";
    @Input() 
    get data(): T {
        return this._data;
    }
    set data(d: T) {
        this._data = d;
        this.dataChange.emit(this._data);
    }
    @Output() dataChange: EventEmitter<T> = new EventEmitter();
    @Output() onValueChanged: EventEmitter<any> = new EventEmitter();
    
    private _data: T;

    public validationGroupName: string = "vg" + (""+Math.random()).split(".")[1].substr(0,8);

    public customComponents: Array<{name: string, component: VSTCustomTemplateComponent<any>}> = [];

    private canPartialSave: boolean = false;

    private stateEventController: VsStateEventController = new VsStateEventController();

    constructor(private injector: Injector) {
        super();
        this.formatChange.subscribe(() => {
            this.handleFormat();
        });
    }

    beforeInitialize() {
        super.beforeInitialize();
        // get default params to form
        const defaultParams = VstGlobalFormConfig();
        smartAssign(this.schema, defaultParams);
    }

    customInitialize(): void {
        // percorrer arvore de items
        this.schema.items.forEach((node) => {
            // RESOLVER GRUPO DE VALIDACAO
            if (!this.dxInstance.validationGroup) this.dxInstance.validationGroup = this.validationGroupName;
            else this.validationGroupName = this.dxInstance.validationGroup;
            // INICIALIZAR VAZIA A LISTA DE COMPONENTES CUSTOMIZADOS
            this.customComponents = [];
            // PERCORRER ARVORE DE ITEMS
            this.jumpIntoNode(node);
        });

        // ativar EBR para formulario
        this.activateEventBaseResults();
        // já verificou os campos, travar eventbasedcontroller
        this.stateEventController.lock();
        // this.dxInstance.onOptionChanged.subscribe((e) => {
        //     // console.log("# onOptionChanged", e);
        // });
        this.dxInstance.onFieldDataChanged.subscribe((e) => {
            if (this.format === "readonly") return;
            setTimeout(() => {
                this.dxInstance.instance.beginUpdate();
                this.stateEventController.notifyFieldDataChanged(e);
                this.onValueChanged.emit(e);
                this.dxInstance.instance.endUpdate();
            });
        });
        this.dxInstance.onContentReady.subscribe((e) => {
            this.stateEventController.runColdStart(e.component);
            if (this.validationGroupName) {
                const validationGroup: {validators: [DevExpress.ui.dxValidator]} = validationEngine.getGroupConfig(this.validationGroupName)
                if (validationGroup && validationGroup.validators && validationGroup.validators.length) {
                    validationGroup.validators.forEach((val) => {
                        val.on("validated", (e: {brokenRule: any, isValid: boolean, name: string, validationRules: any, value: any}) => {
                            // console.log("Validating", e);
                        })
                    })
                }
            }
        })


        setTimeout(() => {
            this.handleFormat();

            ///// VERIFICAÇÃO DE PENDENCIAS DE FORM DATA QUE NAO FORAM COMPLETADAS
            if (this.schema.savePartial && this.schema.savePartial.active) {
                ///// preparacao para salvamentos parciais
                if (this.formOperation === "create") {
                    const delay = this.schema.savePartial.delay || 5000;
                    setTimeout(() => {
                        this.canPartialSave = true;
                    }, delay);
                }

                ///// fazer leitura de dados
                if (this.formOperation === "create" && this.hasPendingLoad()) {
                    if (this.schema.savePartial.loadMode === VsPartialLoadMode.ALWAYS) {
                        this.loadPartialData();
                    } else if (this.schema.savePartial.loadMode === undefined || this.schema.savePartial.loadMode === VsPartialLoadMode.ASK) {
                        this.askToLoadPartialData();
                    }
                }
            }
        });

        /////// IMPLEMENTAÇÃO DE SALVAMENTO DE FORMDATA INCOMPLETO
        if (this.formOperation === "create" && this.schema.savePartial && this.schema.savePartial.active) {
            if (!this.schema.savePartial.saveas) {
                this.schema.savePartial.saveas = this.getIdentificationFromFields(this.schema);
            }
            
            this.dxInstance.onFieldDataChanged.subscribe((e) => {
                // console.log("onFieldDataChanged", e );
                if (e.dataField != undefined && e.value != undefined) {
                    this.savePartial();
                }
            });
        }
    }

    jumpIntoNode(nodeItem: VSTFormItem) {
        // operar node, procurar por templates, ou grupo de array
        // NOTA: BUSCA EM PROFUNDIDADE
        if ((nodeItem as Object).hasOwnProperty('items') && (nodeItem as VstdxFormGroup | VstcdxFormArray).items.length > 0) {
            // entao, cast para items
            ((nodeItem as VstdxFormGroup | VstcdxFormArray).items as VSTFormItems).forEach((node) => {
                this.jumpIntoNode(node);
            });
        }
        // PARA TABS
        else if (nodeItem.itemType === 'tabbed' && (nodeItem as Object).hasOwnProperty('tabs') && (nodeItem as VstdxFormTabbed).tabs.length > 0) {
            // entao, cast para items
            ((nodeItem as VstdxFormTabbed).tabs as VSTFormItems).forEach((node) => {
                this.jumpIntoNode(node);
            });
        }
        // operate, template sao reconhecidos pelo editorType
        else if (((nodeItem as VstdxFormItem).itemType === "simple" || (nodeItem as VstdxFormItem).itemType === undefined) && (nodeItem as Object).hasOwnProperty('editorType')) {
            // look for pattern
            if ((nodeItem as VstdxFormItem).editorType.substr(0, 3) == 'vst') {
                // FOUND! OVERRIDE template!
                (nodeItem as VstdxFormItem).cssClass = "vstcustomcomponent " + ((nodeItem as VstdxFormItem).cssClass || "");
                (nodeItem as VstdxFormItem).template = tieDxComponentTemplate(this.injector, this, (cp, valInstance) => {
                    cp.format = this.format;
                    cp.formatChange.emit(this.format);
                    this.formatChange.subscribe(() => {
                        cp.format = this.format;
                        cp.formatChange.emit(this.format);
                    })
                    this.customComponents.push({name: (nodeItem as VstdxFormItem).editorType, component: cp});

                    //////
                    ////// VALIDACOES 
                    valInstance.validationGroup = cp.validationGroupName;
                    valInstance.adapterGetValue = () => {
                        return cp.value;
                    }

                    const valsRules = [] as Array<DevExpress.ui.RequiredRule | DevExpress.ui.NumericRule | DevExpress.ui.RangeRule | DevExpress.ui.StringLengthRule | DevExpress.ui.CustomRule | DevExpress.ui.CompareRule | DevExpress.ui.PatternRule | DevExpress.ui.EmailRule>;
                    let caption = '';
                    if ((nodeItem as VstdxFormItem).label && (nodeItem as VstdxFormItem).label.text) caption = (nodeItem as VstdxFormItem).label.text;
                    else if ((nodeItem as VstdxFormItem).dataField) caption = (nodeItem as VstdxFormItem).dataField;

                    if ((nodeItem as VstdxFormItem).isRequired) valsRules.push({type: "required", message: caption? caption+" é de preenchimento obrigatório" : undefined});
                    if ((nodeItem as VstdxFormItem).validationRules && (nodeItem as VstdxFormItem).validationRules.length) valsRules.push(...(nodeItem as VstdxFormItem).validationRules);
                    valInstance.validator.validationRules = valsRules;

                    // FIM
                    valInstance.finishKnot();
                });
            }
        }

        //// ITEM SIMPLES
        if (((nodeItem as VstdxFormItem).itemType === "simple" || (nodeItem as VstdxFormItem).itemType === undefined) && (!(nodeItem as VstdxFormItem).editorType || (nodeItem as VstdxFormItem).editorType && (nodeItem as VstdxFormItem).editorType.substr(0, 3) !== 'vst')) {
            this.insertCssInNode(nodeItem as VstdxFormItem);
        }
    }

    activateEventBaseResults() {
        if (this.stateEventController.locked || !this.schema.dynamicOptions) return;

        if (this.schema.dynamicOptions.fieldMap) {
            Object.keys(this.schema.dynamicOptions.fieldMap).forEach((optionPath) => {
                const dyn = this.schema.dynamicOptions.fieldMap[optionPath];
                this.stateEventController.add(dyn, true, optionPath);
            });
        }
        if (this.schema.dynamicOptions.nameMap) {
            Object.keys(this.schema.dynamicOptions.nameMap).forEach((optionPath) => {
                const dyn = this.schema.dynamicOptions.nameMap[optionPath];
                this.stateEventController.add(dyn, false, optionPath);
            });
        }
    }

    handleFormat() {
        this.dxInstance.instance.beginUpdate();
        if (this.format === "edit" || this.format == undefined) {
            const defReadOnly = (this.schema.readOnly) ? this.schema.readOnly : false;
            this.dxInstance.readOnly = defReadOnly;
        } else {
            this.dxInstance.readOnly = true;
        }
        this.customComponents.forEach((item) => {
            if (!this.format) return;
            item.component.format = this.format;
            item.component.formatChange.emit(this.format);
        });
        this.dxInstance.instance.endUpdate();
    }

    transformData() {
        this.dataTrasformation(this.data);
    }

    getIdentificationFromFields(nodeItem: VSTFormItem): string {
        const localDataField = nodeItem['dataField'] || '';
        if ((nodeItem as Object).hasOwnProperty('items') && (nodeItem as VstdxFormGroup).items.length > 0) {
            // entao, cast para items
            return localDataField + ".grp[" + 
                ((nodeItem as VstdxFormGroup).items as VSTFormItems)
                    .map((node) => this.getIdentificationFromFields(node))
                    .join(':')
                + "]";
        }
        // PARA TABS
        else if (nodeItem.itemType === 'tabbed' && (nodeItem as Object).hasOwnProperty('tabs') && (nodeItem as VstdxFormTabbed).tabs.length > 0) {
            // entao, cast para items
            return localDataField + ".tbs[" + 
                ((nodeItem as VstdxFormTabbed).tabs as VSTFormItems)
                    .map((node) => this.getIdentificationFromFields(node))
                    .join(':')
                + "]";
        }
        return localDataField;
    }

    savePartial() {
        if (!this.canPartialSave) return;
        // console.info("Saving Partial FormData as " + this.schema.savePartial.saveas);
        localStorage.setItem(this.schema.savePartial.saveas, JSON.stringify(this.data));
    }

    loadPartialData() {
        // console.info("Loading Partial FormData as " + this.schema.savePartial.saveas);
        let localData: any = localStorage.getItem(this.schema.savePartial.saveas);
        if (localData && typeof localData === "string") {
            localData = JSON.parse(localData);
            this.data = localData;
        } 
    }

    resetPartialData() {
        if (this.schema && this.schema.savePartial && this.schema.savePartial.saveas) {
            localStorage.removeItem(this.schema.savePartial.saveas);
        }
    }

    hasPendingLoad(): boolean {
        if (this.schema && this.schema.savePartial && this.schema.savePartial.saveas) {
            let localData: any = localStorage.getItem(this.schema.savePartial.saveas);
            if (localData && typeof localData === "string") {
                try {
                    localData = JSON.parse(localData);
                    return !!Object.keys(localData).length;
                } catch (error) {
                    return false;
                }
            } 
        }
    }

    async askToLoadPartialData(): Promise<boolean> {
        return new Promise((resolve) => {
            this.popupDataRecoverConfirmation.visible = true;
            const el = document.createElement('p');
            el.style.textAlign = "center";
            el.style.fontWeight = "bold";
            el.textContent = this.schema.savePartial.askMessage || "Observamos que a última inclusão não foi salva. Deseja recuperar os dados?";
            this.popupDataRecoverConfirmation.contentTemplate = el;
            this.popupDataRecoverConfirmation.closeOnOutsideClick = false;
            this.popupDataRecoverConfirmation.toolbarItems = [
                {widget: 'dxButton', location: 'center', toolbar: 'bottom', options: {
                    type: 'normal',
                    text: 'Recuperar',
                    onClick: () => {
                        this.popupDataRecoverConfirmation.visible = false;
                        this.loadPartialData();
                        resolve(false);
                    }
                }},
                {widget: 'dxButton', location: 'center', toolbar: 'bottom', options: {
                    type: 'danger',
                    text: 'Descartar',
                    onClick: () => {
                        this.popupDataRecoverConfirmation.visible = false;
                        this.resetPartialData();
                        resolve(true);
                    }
                }}
            ];
        });
    }
}
