import { Component, OnInit, EventEmitter } from '@angular/core';
import { VSTGridSchema, VsPartialLoadMode, SbrDyn, SbrCon, SbrSel } from '../../../vst-gridform/src/public_api';
import { VstDxForm } from 'projects/vst-gridform/src/lib/vst-form/interfaces/vst-dxform.interface';
import { Subscriber, Subject, Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    sub: Subject<any> = new Subject();
    evm: EventEmitter<any> = new EventEmitter()

    form: VstDxForm;
    schema: VSTGridSchema = {
        keyExpr: 'id',
        editing: {
            allowAdding: true,
            allowUpdating: true,
            // allowDeleting: true,
            allowViewing: true,
            useIcons: true,
            mode: "form",
            forceFormHandler: true,
            cancelButton: {
                askBeforeLeave: true,
            },
            form: {
                savePartial: {
                    active: true,
                    loadMode: VsPartialLoadMode.ASK,
                },
                onInitialized: (e) => {
                    this.form = e.component;
                },
                items: [
                    {itemType: "group", colCount: 4, items: [
                        {dataField: "id", label: {text: "id"}},
                        {dataField: "name", label: {text: "name"}},
                        {dataField: "family", label: {text: "family"}},
                        {dataField: "age", label: {text: "age"}},
                        {dataField: "nick", label: {text: "nick"}},
                        {dataField: "phone", label: {text: "phone"}},
                        {dataField: "mobileModel", label: {text: "mobileModel"}},
                        {dataField: "brand", label: {text: "brand"}, },
                        {dataField: "dev", label: {text: "dev"}},
                        {dataField: "job", label: {text: "job"}},
                    ]},
                    {dataField: "address", editorType: 'vstAddress' as any},
                ],
                dynamicOptions: {
                    fieldMap: {
                        "family.editorOptions.readOnly": SbrDyn.FalseIf(
                            SbrCon.AND(
                                SbrSel.byField('name', 'riheldo'), 
                                SbrSel.byField('id', '$defined'),
                                SbrSel.bySignal(this.evm, ()=>true),
                            )
                        )
                    }
                }
            }
        },
        // masterDetail: {
        //     showHiddenColumns: true,
        // },
        columnChooser: {
            enabled: true,
            mode: "select",
        },
        dataSource: [   
            {id: 1, name: "riheldo", family: "melo", age: "27", nick: "dinho", phone: "61983149995", mobileModel: "galaxy", brand: "samsung", dev: "good", job: "4"},
            {id: 2, name: "riheldo", family: "melo", age: "27", nick: "dinho", phone: "61983149995", mobileModel: "galaxy", brand: "samsung", dev: "good", job: "4"},
            {id: 3, name: "riheldo", family: "melo", age: "27", nick: "dinho", phone: "61983149995", mobileModel: "galaxy", brand: "samsung", dev: "good", job: "4"},
        ],
        columns: [
            {dataField: "id"},
            {dataField: "name"},
            {dataField: "age"},
            {dataField: "nick"},
        ],
        transform: {
            pipes: [{fields: "*", use: "uppercase", exceptions: ["nick"]}]
        },
    }

    ngOnInit() {
        this.sub.next
    }
}
