import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { VSTGridModule } from '../../../vst-gridform/src/public_api';
import { ModAddressModule } from './mod-address/mod-address.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    VSTGridModule,
    ModAddressModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
