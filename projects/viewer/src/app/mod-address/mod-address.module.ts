import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddressComponent } from './address/address.component';
import { VSTTemplatesRegister, VSTFormModule } from '../../../../vst-gridform/src/public_api';;


@NgModule({
    declarations: [
        AddressComponent
    ],
    entryComponents:[
        AddressComponent
    ],
    imports: [
        CommonModule,
        VSTFormModule,
    ]
})
export class ModAddressModule { }
VSTTemplatesRegister.get().addTemplate('vstAddress', AddressComponent);