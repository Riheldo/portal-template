import { Component, OnInit, OnDestroy } from '@angular/core';
import { VSTCustomTemplateComponent, VSTFormSchema, CustomTemplateFormat } from '../../../../../vst-gridform/src/public_api';
import { VstDxForm } from 'projects/vst-gridform/src/lib/vst-form/interfaces/vst-dxform.interface';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent extends VSTCustomTemplateComponent<any> implements OnInit, OnDestroy {
    
    form: VstDxForm
    schema: VSTFormSchema = {
        onInitialized: (e) => {
            this.form = e.component;
        },
        items: [
            {itemType: 'group', colCount: 3, items: [
                {dataField: "cep", label: {text: "CEP"}},
                {dataField: "address", label: {text: "Endereço"}},
                {dataField: "city", label: {text: "Cidade"}},
                {dataField: "neighbohood", label: {text: "Bairro"}},
                {dataField: "state", label: {text: "Estado"}},
                {dataField: "country", label: {text: "País"}},
            ]}
        ]
    }
    
    constructor() {
        super();
    }
    
    ngOnInit() {
        console.log("== AddressComponent OnInit")
        if (!this.value) {
            this.value = {};
            this.valueChange.emit(this.value);
        }
        this.formatChange.subscribe((val: CustomTemplateFormat) => {
            setTimeout(() => {
                if (val === 'readonly') this.form.option('readOnly', true);
                else this.form.option('readOnly', false);
            });
        });
    }

    ngOnDestroy() {
        console.log("==== AddressComponent OnDestroy")
    }
}
