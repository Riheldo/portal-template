import { NgModule, ErrorHandler } from '@angular/core';
import { VSTErrorTracingHandler } from './vst-tracing.service';

@NgModule({
    providers: [{ provide: ErrorHandler, useClass: VSTErrorTracingHandler }]
})
export class VstTracingModule { }
