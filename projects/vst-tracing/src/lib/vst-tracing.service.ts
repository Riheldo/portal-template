import { ErrorHandler } from '@angular/core';

export class VSTErrorTracingHandler extends ErrorHandler {
    handleError(error: TypeError) {
        // console.log("Enviar erro para log de erros.");
        // send the error to the server
        super.handleError(error);
    }
}