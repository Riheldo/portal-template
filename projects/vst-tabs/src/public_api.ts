/*
 * Public API Surface of vst-tabs
 */

export * from './lib/vst-tabs.module';
export * from './lib/components/vst-tabs.component';
export * from './lib/interfaces/vst-routes';
export * from './lib/services/vst-reusestrategy';
export * from './lib/services/vst-tabs.service';