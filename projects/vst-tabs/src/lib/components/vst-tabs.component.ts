import { Component } from '@angular/core';
import { VSTTabsService } from '../services/vst-tabs.service';

@Component({
    selector: 'vst-tabs',
    templateUrl: './vst-tabs.component.html',
    styleUrls: ['./vst-tabs.component.scss', 'vst-tabs-rsp.component.scss'],
})
export class VSTTabsComponent {
    rspTabsShowing: boolean = false;
    constructor(public tabsService: VSTTabsService) {}
}