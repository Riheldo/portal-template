//
// ANGULAR
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouteReuseStrategy, RouterModule } from '@angular/router';

//
// VENDORS
import { DragulaModule } from 'ng2-dragula';

//
// APP
import { VSTCustomReuseStrategy } from './services/vst-reusestrategy';
import { VSTTabsService } from './services/vst-tabs.service';
import { VSTTabsComponent } from './components/vst-tabs.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        DragulaModule,
    ],
    providers: [
        {provide: RouteReuseStrategy, useClass: VSTCustomReuseStrategy, deps: [VSTTabsService]},
    ],
    declarations: [
        VSTTabsComponent,
    ],
    exports: [
        VSTTabsComponent,
    ]
})
export class VSTTabsModule { }
