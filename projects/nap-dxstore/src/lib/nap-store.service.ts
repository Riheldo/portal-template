import { Injectable } from "@angular/core";
import { NAPDataStore } from "./nap-datastore";
import DataSource from "devextreme/data/data_source";
import { NAPConnectionService, NAPFunctionFilter } from "nap-interfaces";


@Injectable()
export class NAPStoreService {

    constructor(private conn: NAPConnectionService) {}
    
    dataStore(resource: string, keys: Array<string> = ["_id"], filter?: Array<any> | NAPFunctionFilter): NAPDataStore {
        return new NAPDataStore(resource, this.conn, keys, filter);
    }

    dataSource(resource: string, keys: Array<string> = ["_id"], filter?: Array<any> | NAPFunctionFilter): DataSource {
        return new DataSource(this.dataStore(resource, keys, filter));
    }
}