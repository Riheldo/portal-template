import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VSTLoginComponent } from './components/vst-login/vst-login.component';
import { VSTRecoverPasswordEmailComponent } from './components/vst-recoverpassword-email/vst-recoverpassword-email.component';
import { Route, RouterModule } from '@angular/router';
import { DxFormModule } from 'devextreme-angular/ui/form';

const routes: Array<Route> = [
    { path: "login", component: VSTLoginComponent },
    { path: "recpw/:token", component: VSTRecoverPasswordEmailComponent }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        DxFormModule
    ],
    declarations: [
        VSTLoginComponent,
        VSTRecoverPasswordEmailComponent,
    ]
})
export class VSTAccessPageModule { }
