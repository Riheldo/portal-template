import { Injectable } from '@angular/core';
import { VSTAcessResponse } from '../interfaces/vst-acess-response';
import { Subject } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class VSTAccessService {

    header: string = "header";
    description: string = "Portal administrativo";
    bgImage: string;
    logoSrc: string;
    logoClass: string = "";

    private _authenticated: boolean = false;
    set authenticated(auth: boolean) {
        if (auth === this._authenticated) return;
        this._authenticated = auth;
        this.authenticatedChange.next(this._authenticated);
    }
    get authenticated(): boolean {
        return this._authenticated;
    }
    authenticatedChange: Subject<boolean> = new Subject();

    checkAuthentication: () => void;
    login: (identity: string, password: string) => Promise<VSTAcessResponse<any>> = () => {
        throw new Error("'VSTAccessService.login' Method not implemented.");
    }
    recoverPasswordByEmail: (email: string) => Promise<VSTAcessResponse<any>> = () => {
        throw new Error("'VSTAccessService.recoverPasswordByEmail' Method not implemented.");
    }
    checkToken: (token: string) => Promise<VSTAcessResponse<any>>  = () => {
        throw new Error("'VSTAccessService.checkToken' Method not implemented.");
    }
    setPasswordForToken: (token: string, password: string) => Promise<VSTAcessResponse<any>> = () => {
        throw new Error("'VSTAccessService.setPasswordForToken' Method not implemented.");
    }
    accessRedirect: () => void = () => {
        throw new Error("'VSTAccessService.accessRedirect' Method not implemented.");
    }
}
