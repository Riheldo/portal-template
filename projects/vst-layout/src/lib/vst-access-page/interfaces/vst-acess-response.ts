export interface VSTAcessResponse<T> {
    status: "success" | "error";
    data: { error: string; msgError: string; } | T;
}
