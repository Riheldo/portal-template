import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import DevExpress from 'devextreme/bundles/dx.all';
import { VSTAccessService } from '../../services/vst-access.service';

@Component({
    selector: 'vst-recoverpassword-email',
    templateUrl: './vst-recoverpassword-email.component.html',
    styleUrls: ['./vst-recoverpassword-email.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class VSTRecoverPasswordEmailComponent implements OnInit {

    tokenValid: boolean = true;
    token: string;
    success: boolean = false;

    formData = {password: '', rPassword: ''}
    formItems: Array<DevExpress.ui.dxFormSimpleItem | DevExpress.ui.dxFormGroupItem | DevExpress.ui.dxFormTabbedItem | DevExpress.ui.dxFormEmptyItem | DevExpress.ui.dxFormButtonItem> = [
        {dataField: 'password', editorOptions: {mode: "password"}, label: {text: 'Senha'}, isRequired: true},
        {dataField: 'rPassword', editorOptions: {mode: "password"}, label: {text: 'Repetir Senha'}, isRequired: true},
        {itemType: 'empty'},
        {itemType: 'button', alignment: 'left', buttonOptions: {text: "Trocar Senha", useSubmitBehavior: true}, cssClass: '_hle-changepwsubmit'},
        {itemType: 'empty'},
    ];
    errMsg="";

    constructor(private route: ActivatedRoute, public accessService: VSTAccessService) { }

    ngOnInit() {
        this.route.params.subscribe((params) => {
            this.token = params.token;
            this.accessService.checkToken(this.token).then((response) => {
                if (response.status === "error") {
                    if ((response.data.errorMessage as string).indexOf("Token Inv") !== -1) this.invalidToken();
                    else this.errMsg = (response.data.errorMessage)?response.data.errorMessage:response.data.error;
                }
            });
        });
    }

    invalidToken() {
        this.tokenValid = false;
    }

    onFormSubmit(e) {
        if (!this.formData.password || !this.formData.rPassword) return false;
        if (this.formData.password !== this.formData.rPassword) {
            this.errMsg = "Senhas não são iguais";
            return false;
        }
        this.errMsg = "";
        this.accessService.setPasswordForToken(this.token, this.formData.password).then((res) => {
            if (res.status == "success") {
                this.success = true;
            } else {
                this.errMsg = (res.data.errorMessage)?res.data.errorMessage:res.data.error;
            }
        });

        return false;
    }
}
