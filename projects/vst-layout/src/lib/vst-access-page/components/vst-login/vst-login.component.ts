import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import DevExpress from 'devextreme/bundles/dx.all';
import { Router } from '@angular/router';
import { VSTAccessService } from '../../services/vst-access.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'vst-login',
    templateUrl: './vst-login.component.html',
    styleUrls: ['./vst-login.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class VSTLoginComponent implements OnInit, OnDestroy {

    formToShow: string = "login";

    formData = {email: '', password: ''}
    formItems: Array<DevExpress.ui.dxFormSimpleItem | DevExpress.ui.dxFormGroupItem | DevExpress.ui.dxFormTabbedItem | DevExpress.ui.dxFormEmptyItem | DevExpress.ui.dxFormButtonItem> = [
        {dataField: 'email', validationRules: [{type: "email"}], label: {text: 'Email'}, isRequired: true},
        {dataField: 'password', editorOptions: {mode: "password"}, label: {text: 'Senha'}, isRequired: true},
        {itemType: 'empty'},
        {itemType: 'button', alignment: 'left', buttonOptions: {text: "Acessar", useSubmitBehavior: true}, cssClass: '_hle-loginsubmit'},
        {itemType: 'empty'},
    ];
    errMsg="";

    recoverPasswordData = {email: ''};
    recoverPasswordFormItems: Array<DevExpress.ui.dxFormSimpleItem | DevExpress.ui.dxFormGroupItem | DevExpress.ui.dxFormTabbedItem | DevExpress.ui.dxFormEmptyItem | DevExpress.ui.dxFormButtonItem> = [
        {dataField: 'email', validationRules: [{type: "email"}], label: {text: 'Email'}, isRequired: true},
        {itemType: 'empty'},
        {itemType: 'button', alignment: 'left', buttonOptions: {text: "Pedir Troca de Senha", useSubmitBehavior: true}, cssClass: '_hle-loginsubmit'},
        {itemType: 'empty'},
    ];
    linkSended: boolean = false;

    authSub: Subscription;

    constructor(private router: Router, public accessService: VSTAccessService) {
        this.authSub = accessService.authenticatedChange.subscribe((auth) => {
            if (auth) {
                try {
                    accessService.accessRedirect();
                } catch (error) {
                    console.warn("Problem to execute accessRedirect ["+error.message+"] error. \nRedirecting router to root.");
                    this.router.navigateByUrl("/");
                }
            }
        })
    }

    ngOnInit() {
        if (this.accessService.checkAuthentication) this.accessService.checkAuthentication();
    }

    ngOnDestroy() {
        this.authSub.unsubscribe();
    }

    forgotPassword() {
        this.formToShow = "recoverPassword";
        this.resetForms();
        return false;
    }

    backToLogin() {
        this.formToShow = 'login';
        this.resetForms();
        return false;
    }
    
    resetForms() {
        this.errMsg = '';
        this.linkSended = false;
        this.recoverPasswordData = {email: ""};
        this.formData = {email: "", password: ""};
    }

    onFormSubmit(e) {
        if (!this.formData.email || !this.formData.password) return false;
        this.errMsg = "";

        this.accessService.login(this.formData.email, this.formData.password).then((res) => {
            if (res.status == "error") {
                this.errMsg = (res.data.errorMessage)?res.data.errorMessage:res.data.error;
            }
        });

        return false;
    }

    onFormSubmitRecoverPassword(e) {
        if (!this.recoverPasswordData.email) return false;
        this.errMsg = "";

        this.accessService.recoverPasswordByEmail(this.recoverPasswordData.email).then((response) => {
            if (response.status == "success") {
                this.linkSended = true;
                this.recoverPasswordData.email = "";
            } else {
                this.errMsg = (response.data.errorMessage)?response.data.errorMessage:response.data.error;
            }
        });

        return false;
    }

}
