import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'vst-header',
    templateUrl: './vst-header.component.html',
    styleUrls: ['./vst-header.component.scss']
})
export class VSTHeaderComponent {

    @Input() title: string = "";
    @Input() description: string = "";
    @Input() class: string = "";

}
