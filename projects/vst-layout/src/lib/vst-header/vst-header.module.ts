import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VSTHeaderComponent } from './vst-header.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [VSTHeaderComponent],
  exports: [VSTHeaderComponent]
})
export class VSTHeaderModule { }
