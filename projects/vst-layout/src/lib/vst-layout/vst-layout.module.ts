// ANGULAR
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations"
import { HttpClientModule } from '@angular/common/http';

// VENDORS
import { DragulaModule } from 'ng2-dragula';
import { VSTTabsModule } from 'vst-tabs';

// COMPONENTS
import { VSTLayoutComponent } from './vst-layout.component';
import { VSTFilterMenuComponent } from './components/vst-menu/vst-filter-menu.component';
import { VSTMenuTreeComponent } from './components/vst-menu/vst-menutree.component';
import { VSTSideMenuComponent } from './components/vst-menu/vst-sidemenu.component';
import { VSTTreeTitleComponent } from './components/vst-menu/vst-treetitle.component';
import { VSTBreadcrumbsComponent } from './components/vst-breadcrumbs/vst-breadcrumbs.component';
import { VSTLayoutService } from './services/vst-layout.service';
import { VstAuthWidgetComponent } from './components/vst-auth-widget/vst-auth-widget.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        BrowserAnimationsModule,
        HttpClientModule,
        
        // intern vendor
        VSTTabsModule,
        
        // extern vendor
        DragulaModule,
    ],
    declarations: [
        VSTLayoutComponent,
        VSTFilterMenuComponent,
        VSTMenuTreeComponent,
        VSTSideMenuComponent,
        VSTTreeTitleComponent,
        VSTBreadcrumbsComponent,
        VstAuthWidgetComponent,
    ],
    exports: [
        VSTLayoutComponent,
        VSTFilterMenuComponent,
        VSTMenuTreeComponent,
        VSTSideMenuComponent,
        VSTTreeTitleComponent,
        VSTBreadcrumbsComponent,
    ],
    providers: [
        VSTLayoutService
    ]
})
export class VSTLayoutModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: VSTLayoutModule,
            providers: [VSTLayoutService]
        };
    }
}
