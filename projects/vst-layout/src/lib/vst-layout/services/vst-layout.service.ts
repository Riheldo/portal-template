import { Injectable } from '@angular/core';
import { VSTItemMenu } from '../interfaces/menu.interfaces';
import { VstAuthWidgetOptionsI } from '../components/vst-auth-widget/vst-auth-widget-options.interface';

@Injectable()
export class VSTLayoutService {
    filterEnabled: boolean = true;
    tabsEnabled: boolean = true;
    breadcumbsEnabled: boolean = true;
    authWidgetEnable: boolean = false;
    menuMarkItem: boolean = false;
    groups: Array<VSTItemMenu> = [];
    logoSrc: string = "";
    authWidgetOptions: VstAuthWidgetOptionsI = {};
}