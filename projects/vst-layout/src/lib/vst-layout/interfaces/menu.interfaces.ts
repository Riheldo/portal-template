export interface VSTItemMenu {
    id?: string;
    class?: string | Array<string>;
    title: string;
    iconSrc?: string;
    iconClass?: string;
    pathUrl?: string;
    open?: boolean;
    canClose?: boolean;
    items?: Array<VSTItemMenu>;
    
    onClick?: (item?: VSTItemMenu, event?: any) => any;
}