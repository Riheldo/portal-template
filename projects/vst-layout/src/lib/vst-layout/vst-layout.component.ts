import { Component, OnInit } from '@angular/core';
import { VSTLayoutService } from './services/vst-layout.service';
import { VSTTabsService } from 'vst-tabs';

@Component({
    selector: 'vst-layout',
    templateUrl: './vst-layout.component.html',
    styleUrls: ['./vst-layout.component.scss']
})
export class VSTLayoutComponent implements OnInit {

    menuOpen: boolean = false;
    filterActivated: boolean = false;

    constructor(public tabsService: VSTTabsService, public layoutService: VSTLayoutService) { }

    ngOnInit() {
        
    }

}
