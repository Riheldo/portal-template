import { Component } from '@angular/core';
import { VstAuthWidgetI } from './vst-auth-widget.interface';
import { Router } from '@angular/router';
import { VSTLayoutService } from '../../services/vst-layout.service';

@Component({
    selector: 'vst-auth-widget',
    templateUrl: './vst-auth-widget.component.html',
    styleUrls: ['./vst-auth-widget.component.scss']
})
export class VstAuthWidgetComponent {

    constructor(private router: Router, public layoutService: VSTLayoutService) { }

    optionClicked(option: VstAuthWidgetI, e: any) {
        if (option.pathUrl) return this.router.navigateByUrl(option.pathUrl);
        if (typeof option.onClick === "function") return option.onClick(option, e);
    }

}
