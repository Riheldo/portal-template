import { VstAuthWidgetI } from './vst-auth-widget.interface';

export interface VstAuthWidgetOptionsI {
    title?: string;
    subTitle?: string;
    image?: string;
    options?: Array<VstAuthWidgetI>;
}
