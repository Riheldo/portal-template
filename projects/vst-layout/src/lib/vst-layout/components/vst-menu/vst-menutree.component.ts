import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import {
    trigger,
    state,
    style,
    animate,
    transition
} from '@angular/animations';
import { VSTItemMenu } from '../../interfaces/menu.interfaces';
import { ActivatedRoute, RouterState, NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';

function actualUrlPath(activatedRoute: ActivatedRoute): string {
    return (activatedRoute['_routerState'] as RouterState).snapshot.url;
}

@Component({
    selector: 'vst-menutree',
    templateUrl: './vst-menutree.component.html',
    styleUrls: ['./vst-menutree.component.scss'],
    animations: [
        trigger('openChanged', [
            state('true', style({height: '*', overflow: 'hidden'})),
            state('false', style({height: '0', overflow: 'hidden'})),
            transition('1 => 0', animate('200ms ease-in')),
            transition('0 => 1', animate('200ms ease-out'))
        ])
    ]
})
export class VSTMenuTreeComponent implements OnInit {
    
    @Input() itemsMenu: Array<VSTItemMenu>;
    @Input() markItemActive: boolean = false;
    activatedItem: VSTItemMenu = undefined;
    itemMarkedEventSub: Subscription;
    
    constructor(private router: Router, private route: ActivatedRoute) {}

    ngOnInit() {
        this.itemsMenu.forEach(itemMenu => {
            // itemMenu.pathUrl
            // itemMenu.iconSrc
            itemMenu.canClose = (typeof itemMenu.canClose === 'undefined') ? true : itemMenu.canClose;
            itemMenu.class = (typeof itemMenu.class === 'undefined') ? '' : itemMenu.class;
            itemMenu.id = (typeof itemMenu.id === 'undefined') ? '' : itemMenu.id;
            itemMenu.open = (typeof itemMenu.open === 'undefined') ? true : itemMenu.open;
            // itemMenu.items = 
        });
        // this.itemMenu.canClose = (typeof this.itemMenu.canClose === 'undefined') ? true : this.itemMenu.canClose;
        // this.itemMenu.class = (typeof this.itemMenu.class === 'undefined') ? '' : this.itemMenu.class;
        // this.itemMenu.id = (typeof this.itemMenu.id === 'undefined') ? '' : this.itemMenu.id;
        // this.itemMenu.open = (typeof this.itemMenu.open === 'undefined') ? true : this.itemMenu.open;
        // this.itemMenu.items.forEach(item => {
        //     if (!item.class) item.class = '';
        //     if (!item.id) item.id = '';
        //     if (!item.iconSrc) item.iconSrc = '';
        // });
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.markItemActive && changes.markItemActive.previousValue != changes.markItemActive.currentValue) {
            if (changes.markItemActive.currentValue && !this.itemMarkedEventSub) {
                this.itemMarkedEventSub = this.router.events.subscribe((routerEvent) => {
                    if (routerEvent instanceof NavigationEnd) {
                        this.activatedItem = null;
                        this.defineActivatedAnchor();
                    }
                });
                this.defineActivatedAnchor();
            } else if (!changes.markItemActive.currentValue && this.itemMarkedEventSub) {
                this.itemMarkedEventSub.unsubscribe();
            }
        }
    }

    itemMeuClicked(event, itemMenu: VSTItemMenu) {
        if (itemMenu && typeof itemMenu.onClick === "function") {
            return itemMenu.onClick(itemMenu, event);
        }
    }

    toggleTree(itemMenu: VSTItemMenu) {
        if (!itemMenu.canClose) return;
        itemMenu.open = !itemMenu.open;
    }

    defineActivatedAnchor() {
        const actualUrl = actualUrlPath(this.route);
        for (let item of this.itemsMenu) {
            if (item.pathUrl == actualUrl) {
                this.activatedItem = item;
                break;
            }
        }
    }

}