import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'vst-treetitle',
    templateUrl: './vst-treetitle.component.html',
    styleUrls: ['./vst-treetitle.component.scss']
})
export class VSTTreeTitleComponent {
    @Input() open: boolean = false;
    @Input() canClose: boolean = false;
    @Input() title: string = 'Titulo';
    @Input() pathUrl: string;
    constructor(private router: Router) {}
    clicked(e) {
        if (this.pathUrl) this.router.navigateByUrl(this.pathUrl);
    }
}
