import { Component, Input, OnInit } from '@angular/core';
import { VSTItemMenu } from '../../interfaces/menu.interfaces';


@Component({
    selector: 'vst-sidemenu',
    templateUrl: './vst-sidemenu.component.html',
    styles: []
})
export class VSTSideMenuComponent {
    @Input() groups: Array<VSTItemMenu> = [];
    @Input() enableSearch: boolean = false;
    filterActivated: boolean = false;
}
