import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OtherInstitutionsComponent } from './other-institutions.component';
import { VSTTemplatesRegister } from 'projects/vst-gridform/src/public_api';
import { DxValidatorModule } from 'devextreme-angular/ui/validator';

@NgModule({
    imports: [
        CommonModule,
        DxValidatorModule
    ],
    declarations: [OtherInstitutionsComponent],
    entryComponents: [OtherInstitutionsComponent]
})
export class OtherInstitutionsModule { }
VSTTemplatesRegister.get().addTemplate("vst-other-institutions", OtherInstitutionsComponent);