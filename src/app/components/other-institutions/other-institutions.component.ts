import { Component, OnInit, ViewChild, HostBinding, OnDestroy } from '@angular/core';
import { VSTCustomTemplateComponent } from 'projects/vst-gridform/src/public_api';
import { DxValidatorComponent } from 'devextreme-angular/ui/validator';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'other-institutions',
    templateUrl: './other-institutions.component.html',
    styleUrls: ['./other-institutions.component.scss']
})
export class OtherInstitutionsComponent extends VSTCustomTemplateComponent<any> implements OnInit, OnDestroy {

    // @ViewChild(DxValidatorComponent, {static: true}) dxValidator: DxValidatorComponent;
    @HostBinding("style") hostStyle;

    constructor(private sanitizer: DomSanitizer) {
        super();
    }

    ngOnInit() {
        console.log("+ OtherInstitutionsComponent");
        // this.dxValidator.validationGroup = this.validationGroupName;
        // this.dxValidator.validationRules = [{type: "required", message: "Inst eh obrigatorio"}]
        // this.dxValidator.adapter = {
        //     getValue: () => this.value,
        //     applyValidationResults: (e) => {
        //         if (e.isValid) {
        //             // this.hostStyle = "";
        //         } else {
        //             // this.hostStyle = this.sanitizer.bypassSecurityTrustStyle("border: 1px solid #f0bbb9; position: relative; display: block; border-radius: 4px;");
        //         }
        //     }
        // }
    }

    ngOnDestroy() {
        console.log("- OtherInstitutionsComponent");
    }
}
