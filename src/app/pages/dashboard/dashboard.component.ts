import { Component, OnInit } from '@angular/core';
import { VSTTabsService } from 'vst-tabs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private tabs: VSTTabsService) { }

  ngOnInit() {
    this.tabs.activatedTab().title = "Mudando a titla";
  }

}
