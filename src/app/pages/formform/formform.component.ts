import { Component, OnInit, OnDestroy } from '@angular/core';
import { VSTFormSchema, VSTGridSchema } from 'projects/vst-gridform/src/public_api';
import { VstDxForm } from 'projects/vst-gridform/src/lib/vst-form/interfaces/vst-dxform.interface';

@Component({
    selector: 'app-formform',
    templateUrl: './formform.component.html',
    styleUrls: ['./formform.component.scss']
})
export class FormformComponent implements OnInit, OnDestroy {

    form: VstDxForm;

    formData = {};
    formSchema: VSTFormSchema = {
        onInitialized: (e) => {
            this.form = e.component
        },
        labelLocation: "top",
        items: [
            { itemType: "group", colCount: 2, items: [
                { dataField: "institutionId", label: {text: "Instituição"}, isRequired: true },
                { dataField: "name", label: {text: "Nome"}, isRequired: true },
                { dataField: "returnPerc", label: {text: "Percentual de Retorno"}, editorType: "dxNumberBox", isRequired: true },
                { dataField: "onenetPerc", label: {text: "Percentual comp"}, editorType: "dxNumberBox", isRequired: true },
                { dataField: "sellerPerc", label: {text: "Percentual Vendedor"}, editorType: "dxNumberBox", isRequired: true },
                // { dataField: "institutionPerc", label: {text: "Percentual Instituição"} },
                { dataField: "institutionPerc", label: {text: "Regulagem Percentual Instituição"}, editorType: "vst-other-institutions" as any, isRequired: true, validationRules: [{type: "custom", validationCallback: (e) => {
                    console.log("Validacao customizada para componente customizado", e.value);
                    return true;
                }}] },
            ]},
            {itemType: "button", buttonOptions: {useSubmitBehavior: true, text: "Enviar"}}
        ],
        showValidationSummary: true,
        onContentReady: (e) => {
            console.log("onContentReady", e.component.option());
        }
    };

    gridSchema: VSTGridSchema = {
        editing: {
            allowAdding: true,
            allowDeleting: true,
            allowUpdating: true,
            useIcons: true,
            mode: "form",
            forceFormHandler: true,
            form: {
                items: [
                    { itemType: "group", colCount: 2, items: [
                        { dataField: "institutionId", label: {text: "Instituição"}, isRequired: true },
                        { dataField: "name", label: {text: "Nome"}, isRequired: true },
                        { dataField: "returnPerc", label: {text: "Percentual de Retorno"}, editorType: "dxNumberBox", isRequired: true },
                        { dataField: "onenetPerc", label: {text: "Percentual 1Net"}, editorType: "dxNumberBox", isRequired: true },
                        { dataField: "sellerPerczz", label: {text: "Percentual Vendedor"}, editorType: "dxNumberBox", isRequired: true },
                        // { dataField: "institutionPerc", label: {text: "Percentual Instituição"} },
                        { dataField: "institutionPerc", label: {text: "Regulagem Percentual Instituição"}, editorType: "vst-other-institutions" as any, isRequired: true, validationRules: [{type: "custom", validationCallback: (e) => {
                            console.log("Validacao");
                            return true;
                        }}] },
                    ]},
                ]
            }
        },
        dataSource: [],
        // dataSource: this.store.dataSource("generic", ["id"]),
        columns: [
            { dataField: "institutionId", caption: "institutionId" },
            { dataField: "name", caption: "name" },
            { dataField: "returnPerc", caption: "returnPerc" },
            { dataField: "onenetPerc", caption: "onenetPerc" },
            { dataField: "sellerPerc", caption: "sellerPerc" },
            { dataField: "institutionPerc", caption: "institutionPerc" },
        ]
    };

    
    // constructor(private conn: NAPConnectionService, private store: NAPStoreService) { }
    
    submit() {
        console.log("Submit fired!");
        return false;
    }

    ngOnInit() {
        console.log('oninit')
        // setTimeout(() => {
        //     this.form.setChanges(() => {
        //         this.form.setItemField('returnPerc', 'label.text', 'TROCA DE TEXTO 1');
        //     })
        // });
        // setTimeout(() => {
        //     this.form.setChanges(() => {
        //         this.form.setItemField('institutionPerc', 'label.text', 'TROCA DE TEXTO 2');
        //     })
        // }, 4000);
    }

    ngOnDestroy() {
        console.log("Destroynf formform")
    }

}
