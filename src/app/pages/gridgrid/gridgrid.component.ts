import { Component, OnInit, ViewChild } from '@angular/core';
import { VSTGridSchema, VSTGridComponent } from 'projects/vst-gridform/src/public_api';

@Component({
    selector: 'app-gridgrid',
    templateUrl: './gridgrid.component.html',
    styleUrls: ['./gridgrid.component.css']
})
export class GridgridComponent implements OnInit {

    @ViewChild(VSTGridComponent, {static: true}) grid: VSTGridComponent;

    schema: VSTGridSchema = {
        dataSource: [],
        editing: {
            allowAdding: true,
            forceFormHandler: true,
            form: {
                items: []
            }
        }
    }

    ngOnInit() {
        this.grid.dxInstanceChanges.subscribe(() => {
            console.log("dxInstanceChanges", this.grid.dxInstance);
        })
    }
}
