//
//// ANGULAR
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router'
import { HttpClientModule } from '@angular/common/http';

//
//// INNER VENDORS
import { VSTLayoutComponent, VSTLayoutModule, VSTHeaderModule, VSTAccessPageModule } from 'projects/vst-layout/src/public_api';
import { VSTRoutes } from 'vst-tabs';
import { VSTGridModule, VSTFormModule } from 'projects/vst-gridform/src/public_api';

//
//// EXTERN VENDORS
import { DxFormModule } from 'devextreme-angular/ui/form';
import { DxFileUploaderModule } from 'devextreme-angular/ui/file-uploader';
import { DxDataGridModule } from 'devextreme-angular/ui/data-grid';
import { DxTextAreaModule } from 'devextreme-angular/ui/text-area';
import { DxValidatorModule } from 'devextreme-angular/ui/validator';
import { DragulaModule } from 'ng2-dragula';

//
//// APP
import { AppComponent } from './app.component';
import { AuthGuard } from './models/AuthGuard';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { FormformComponent } from './pages/formform/formform.component';
import { NAPConnectionService } from 'projects/nap-interfaces/src/public_api';
import { NAPStoreService } from 'projects/nap-dxstore/src/public_api';
import { OtherInstitutionsModule } from './components/other-institutions/other-institutions.module';
import { GridgridComponent } from './pages/gridgrid/gridgrid.component';


const routes: VSTRoutes = [
    { path: "", component: VSTLayoutComponent, canActivate: [AuthGuard], children: [
        { path: "dashboard", component: DashboardComponent, openTab: true, title: "Dashboard" }, // some dashboardcomponent
        { path: "dashboard2", component: DashboardComponent, openTab: true, title: "Dashboard 2" }, // some dashboardcomponent
        { path: "dashboard3", component: DashboardComponent, openTab: true, title: "Dashboard 3" }, // some dashboardcomponent
        { path: "formform", component: FormformComponent, title: "Form" }, // some dashboardcomponent
        { path: "gridgrid", component: GridgridComponent, openTab: true, title: "Grid" }, // some dashboardcomponent
        { path: "", redirectTo: "dashboard", pathMatch: "full"}
    ]},
];


@NgModule({
    declarations: [
        AppComponent,
        DashboardComponent,
        FormformComponent,
        GridgridComponent,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        RouterModule.forRoot(routes),
        VSTLayoutModule.forRoot(),
        VSTHeaderModule,
        VSTAccessPageModule,
        DragulaModule.forRoot(),

        VSTGridModule,
        VSTFormModule,
        
        OtherInstitutionsModule,

        // devextreme componentes
        // DxFormModule,
        // DxFileUploaderModule,
        // DxDataGridModule,
        // DxTextAreaModule,
        // DxValidatorModule,
    ],
    providers: [
        // NAPConnectionService,
        // NAPStoreService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
