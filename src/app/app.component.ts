import { Component, OnInit } from '@angular/core';

// lang
// exec
import 'devextreme/localization/globalize/number';
import 'devextreme/localization/globalize/date';
import 'devextreme/localization/globalize/currency';
import 'devextreme/localization/globalize/message';

// messages
import ptMessages from 'devextreme/localization/messages/pt.json';
import ptCldrData from 'devextreme-cldr-data/de.json';
import supplementalCldrData from 'devextreme-cldr-data/supplemental.json';

// widget
// import ptCaGregorian from 'cldr-data/main/pt/ca-gregorian.json';
import ptNumbers from 'cldr-data/main/pt/numbers.json';
import ptCurrencies from 'cldr-data/main/pt/currencies.json';

// suplemental
// import likelySubtags from 'cldr-data/supplemental/likelySubtags.json';
// import timeData from 'cldr-data/supplemental/timeData.json';
// import weekData from 'cldr-data/supplemental/weekData.json';
import currencyData from 'cldr-data/supplemental/currencyData.json';
import numberingSystems from 'cldr-data/supplemental/numberingSystems.json';

import Globalize from 'globalize';
import { VSTLayoutService } from 'projects/vst-layout/src/public_api';
import { VSTAccessService } from 'projects/vst-layout/src/public_api';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    constructor(private layout: VSTLayoutService) {}
    ngOnInit() {
        Globalize.load(
            ptCldrData,
            supplementalCldrData,
            numberingSystems,
            currencyData,
            ptCurrencies,
            ptNumbers
        );
        Globalize.loadMessages(ptMessages);
        Globalize.locale("pt");
        
        // layout
        this.layout.groups = [
            {title: "Dashboard", pathUrl: "/dashboard", iconSrc: "/assets/megaphone.png"},
            {title: "Menu 2", pathUrl: "/dashboard2",},
            {title: "Menu 2", pathUrl: "/dashboard3",}, 
            {title: "Form", pathUrl: "/formform",}, 
            {title: "Grid", pathUrl: "/gridgrid",}, 
        ];
        this.layout.menuMarkItem = true;
        
        // this.accessService.header = "TEMPLATE";
        // this.accessService.logoSrc = "/assets/logo.jpg";
        // this.accessService.logoClass = "main-logo-center";
        // this.accessService.description = "descricao bkjabskjdjkashdkjads";
        // this.accessService.bgImage = "https://images.pexels.com/photos/67636/rose-blue-flower-rose-blooms-67636.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260";


        this.layout.authWidgetEnable = true;
        this.layout.authWidgetOptions = {
            title: "Algum Titulo",
            subTitle: "Igreja Batista Alagoinha",
            image: "https://www.remove.bg/images/samples/combined/s1.jpg",
            options: [
                {title: "Ver Perfil"},
                {title: "Sair", iconClass: "icofont-logout"}
            ]
        }
    }
}
