import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute } from '@angular/router';

@Injectable({
    providedIn: "root"
})
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private route: ActivatedRoute) { }

    loginRedirect() {
        let next: string = window.location.pathname;
        if (next == "/") next = "";
        let querynext = {_next: window.location.pathname};
        this.router.navigate(["/login"], {queryParams: querynext});
    }

    testValidUser(): boolean {
        // if (false) this.loginRedirect();
        return true;
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.testValidUser();
    }
}
